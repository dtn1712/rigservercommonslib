package com.rockitgaming.server.commons.db;

//import org.jooq.exception.DataAccessException;

public interface DAO<T, I> {

//   /**
//    * Single persist. Use insert on duplicate update. Unique to mysql.
//    *
//    * @param obj record to persist
//    * @return always returns true
//    */
//   public boolean persist(T obj);
//
//   /**
//    * Collection persist. Use insert on duplicate update. Unique to mysql.
//    *
//    * @param objs records to persist
//    * @return number of records persisted
//    */
//   public int persistAll(Collection<T> objs);
//
//   /**
//    * Inserts a single record. Populates ID if it is not present.
//    *
//    * @param obj record to insert
//    * @return number of records inserted
//    */
//   public int insert(T obj);
//
//   /**
//    * Inserts multiple records. Populates ID if it is not present.
//    *
//    * @param objs records to insert
//    * @return number of records inserted
//    */
//   public int insertAll(Collection<T> objs);
//
//   /**
//    * Updates a single record.
//    *
//    * @param obj record to update
//    * @return number of records updated
//    */
//   public int update(T obj);
//
//   /**
//    * Updates multiple records. Use insert on duplicate update as
//    * this is the only way to bulk update objects in one query.
//    *
//    * @param objs records to update
//    * @return number of records updated
//    */
//   public int updateAll(Collection<T> objs);
//
//   /**
//    * Removes one record.
//    *
//    * @param obj record to remove
//    * @return number of records removed
//    */
//   public int remove(T obj);
//
//   /**
//    * Remove all objects in one query. Check for values extracted from a record
//    * key as part of a field set. Works with single column and multicolumn keys.
//    *
//    * @param objs records to remove
//    * @return number of records removed
//    */
//   public int removeAll(Collection<T> objs);
//
//   /**
//    * Removes all records in the table.
//    *
//    * @return number of records removed
//    */
//   public int removeAll();
//
//   /**
//    * Find a single record by ID.
//    *
//    * @param id ID of record to find
//    * @return record which matched the ID
//    */
//   public T findById(I id);
//
//   /**
//    * Find multiple records by ID.
//    *
//    * @param ids IDs of records to find
//    * @return list of records matching the IDs
//    * @throws DataAccessException
//    */
//   public List<T> findByIds(List<I> ids) throws DataAccessException;
//
//   /**
//    * Gets all records in the table.
//    *
//    * @return list of records in the table
//    */
//   public List<T> findAll();
//
//   /**
//    * Removes a single record by ID.
//    *
//    * @param id ID of record to remove
//    * @return number of records removed
//    */
//   public int removeById(I id);
//
//   /**
//    * Remove multiple records by ID.
//    *
//    * @param ids IDs of records to remove
//    * @return number of records removed
//    */
//   public int removeByIds(List<I> ids);
}

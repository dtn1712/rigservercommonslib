package com.rockitgaming.server.commons.db;

//import org.apache.commons.dbcp.BasicDataSource;
//import org.jooq.Configuration;
//import org.jooq.SQLDialect;
//import org.jooq.exception.DataAccessException;
//import org.jooq.impl.DefaultConfiguration;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.sql.DataSource;
//import java.beans.PropertyVetoException;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;

public class DBAccessor {

//   private static final String VALIDATION_QUERY = "SELECT 1";
//   public static final int DEADLOCK_CODE = 1205;
//
//   public static final String CLOSED_POOL_EXCEPTION =
//      "can\'t get connection from closed pool";
//
//   /**
//    * database pool for application. Access controlled through synchronized
//    * methods to make Thread Safe
//    */
//   private static DataSource mDatabasePool = null;
//
//   protected static Map<Connection, ConnectionInfo> mConnections =
//      new ConcurrentHashMap<Connection, ConnectionInfo>();
//
//   private static Logger LOGGER = LoggerFactory.getLogger(DBAccessor.class);
//
//   /**
//    * Initializes DatabasePool
//    *
//    * @throws ClassNotFoundException
//    * @throws PropertyVetoException
//    */
//   public static void initDatabasePool(String dbURL, String driver,
//      String user, String password) throws ClassNotFoundException {
//      BasicDataSource source = new BasicDataSource();
//      source.setDriverClassName(driver);
//      source.setUrl(dbURL);
//      source.setUsername(user);
//      source.setMaxActive(16);
//
//      // TODO Make configurable
//      source.setRemoveAbandoned(true);
//      source.setLogAbandoned(true);
//      source.setMaxWait(60000);
//      if (password != null)
//         source.setPassword(password);
//      source.setValidationQuery(VALIDATION_QUERY);
//      mDatabasePool = source;
//   }
//
//   public static void initSecureDatabasePool(String dbURL, String driver,
//      String user) throws ClassNotFoundException, PropertyVetoException {
//      initDatabasePool(dbURL, driver, user, null);
//   }
//
//   public static boolean isDatabasePoolSet() {
//      return mDatabasePool != null;
//   }
//
//   public static Configuration getDefaultConfiguration() {
//      if (!isDatabasePoolSet())
//         throw new DataAccessException(
//            "Database pool is not initialized. Cannot return a Jooq configuration");
//      return new DefaultConfiguration().set(
//         new ThreadLocalDataSourceConnectionProvider(mDatabasePool)).set(
//         SQLDialect.MYSQL);
//   }
//
//   public static synchronized Connection getConnection(String caller)
//      throws SQLException {
//      Connection conn = null;
//      if (mDatabasePool != null) {
//         try {
//            conn = mDatabasePool.getConnection();
//         }
//         catch (SQLException e) {
//            if (e.getMessage() != null
//               && e.getMessage().startsWith(CLOSED_POOL_EXCEPTION))
//               mDatabasePool = null;
//            else
//               throw e;
//         }
//      }
//
//      mConnections.put(conn, new ConnectionInfo("DBU-" + caller,
//         (new java.util.Date()).getTime(), conn));
//
//      if (!conn.getAutoCommit()) {
//         conn.rollback();
//         conn.setAutoCommit(true);
//      }
//
//      return conn;
//   }
//
//   public static void closeConnection(Connection conn) {
//      ConnectionInfo info;
//      int level;
//      if (conn != null) {
//         info = mConnections.get(conn);
//
//         if (info == null)
//            info = new ConnectionInfo("Unknown", 0, conn);
//
//         try {
//            if (!conn.getAutoCommit())
//               conn.rollback();
//         }
//         catch (Exception err) {
//            LOGGER.info("DBU:{}: Transaction " + "did not rollback",
//               info.mCaller, err);
//         }
//
//         try {
//            if (!conn.getAutoCommit()) {
//               LOGGER.warn(
//                  "DBU:{}: returned Connection with autoCommit set to FALSE",
//                  info.mCaller);
//            }
//         }
//         catch (Exception err) {
//            LOGGER.warn("DBU:{}: Could not get autoCommit status ",
//               info.mCaller, err);
//         }
//
//         try {
//            conn.setAutoCommit(true);
//         }
//         catch (Exception err) {
//            LOGGER.warn("DBU:{}: Could not reset " + "autoCommit status",
//               info.mCaller, err);
//         }
//
//         try {
//            if ((level = conn.getTransactionIsolation()) != Connection.TRANSACTION_REPEATABLE_READ) {
//               LOGGER.warn(
//                  "DBU:{}: Transaction returned in {} mode, resetting to default",
//                  info.mCaller, getIsolationDesc(level));
//            }
//         }
//         catch (Exception err) {
//            LOGGER.warn("DBU:{}: Could not get isolation level", info.mCaller,
//               err);
//         }
//
//         try {
//            conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
//         }
//         catch (Exception err) {
//            LOGGER.error("DBU:{}: Could not reset "
//               + "isolation level because: ", info.mCaller, err);
//         }
//
//         try {
//            mConnections.remove(conn);
//
//            conn.close();
//
//            LOGGER.debug("DBU:{}: Connection sucessfully " + "closed",
//               info.mCaller);
//         }
//         catch (Exception err) {
//            LOGGER.error("DBU:{}: Could not close connection", info.mCaller,
//               err);
//         }
//      }
//   }
//
//   public static String getIsolationDesc(int level) {
//      if (level == Connection.TRANSACTION_NONE)
//         return "NONE";
//      else if (level == Connection.TRANSACTION_READ_COMMITTED)
//         return "Read Committed";
//      else if (level == Connection.TRANSACTION_READ_UNCOMMITTED)
//         return "Read Uncommitted";
//      else if (level == Connection.TRANSACTION_REPEATABLE_READ)
//         return "Repeatable Read";
//      else if (level == Connection.TRANSACTION_SERIALIZABLE)
//         return "Serializable";
//      else
//         return "Unknown";
//   }
//
//   public static void closeResultSet(ResultSet rs) throws SQLException {
//      if (rs != null)
//         rs.close();
//   }
//
//   public static void closeStatement(Statement sqlStatement)
//      throws SQLException {
//      if (sqlStatement != null)
//         sqlStatement.close();
//   }
//
//   public static Map<Connection, ConnectionInfo> getConnectionStates() {
//      return new HashMap<Connection, ConnectionInfo>(mConnections);
//   }
//
//   public static void startTransaction(Connection conn) throws SQLException {
//      startTransaction(conn, Connection.TRANSACTION_REPEATABLE_READ);
//   }
//
//   public static void startTransaction(Connection conn, int isolationLevel)
//      throws SQLException {
//      conn.setAutoCommit(false);
//      conn.setTransactionIsolation(isolationLevel);
//   }
//
//   public static void commitTransaction(Connection conn) throws SQLException {
//      conn.commit();
//      conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
//      conn.setAutoCommit(true);
//   }
//
//   public static void rollbackTransaction(Connection conn) throws SQLException {
//      if (conn == null)
//         return;
//
//      if (!conn.getAutoCommit())
//         conn.rollback();
//
//      conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
//      conn.setAutoCommit(true);
//   }
//
//   public static Integer getNullInteger(ResultSet rs, String name)
//      throws SQLException {
//      Integer result;
//
//      result = Integer.valueOf(rs.getInt(name));
//      if (rs.wasNull())
//         result = null;
//
//      return result;
//   }
//
//   public static Integer getNullInteger(ResultSet rs, int index)
//      throws SQLException {
//      Integer result;
//
//      result = Integer.valueOf(rs.getInt(index));
//      if (rs.wasNull())
//         result = null;
//
//      return result;
//   }
//
//   public static int getTranCount(Connection conn) throws SQLException {
//      ResultSet rs = null;
//      java.sql.PreparedStatement sql = null;
//      try {
//         sql = conn.prepareStatement("SELECT @@TRANCOUNT");
//         rs = sql.executeQuery();
//         if (rs.next())
//            return rs.getInt(1);
//         return 0;
//      }
//      finally {
//         closeResultSet(rs);
//         closeStatement(sql);
//      }
//   }
//
//   public static void verifyTransactionConsistentState(Connection conn)
//      throws SQLException {
//      if (conn.getAutoCommit() != (getTranCount(conn) == 0))
//         throw new SQLException("Database connection state does not match "
//            + "JDBC connection state.  Retry the transaction.",
//            "INTERNAL_ERROR", DEADLOCK_CODE);
//   }
//
//   public static class ConnectionInfo {
//      public String mCaller;
//      public long mCreateTime;
//      public Connection mConnection;
//
//      public ConnectionInfo(String caller, long time, Connection conn) {
//         mCaller = caller;
//         mCreateTime = time;
//         mConnection = conn;
//      }
//
//      public long aliveTime() {
//         return (new java.util.Date()).getTime() - mCreateTime;
//      }
//
//      public String isOpen() {
//         try {
//            if (!mConnection.isClosed()) {
//               return "Open";
//            }
//         }
//         catch (Exception e) {
//            return "Closed";
//         }
//         return "Closed";
//      }
//   }

}

package com.rockitgaming.server.commons.network.netty.core.codec;

/**
 * Codec configuration for TCP frames. Supported types can be found at {@link CodecType}.
 */
public interface CodecConfiguration {

    /**
     * Codec type of current configuration.
     *
     * @return Configuration codec type.
     */
    CodecType getType();

    /**
     * Supported codec types for TCP frames.
     */
    enum CodecType {
        DELIMITED, FIXED_LENGTH
    }
}

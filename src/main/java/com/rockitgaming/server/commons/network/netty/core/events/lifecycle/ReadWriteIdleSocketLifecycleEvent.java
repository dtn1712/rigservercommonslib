package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that read-write idle timeout has elapsed.
 */
public class ReadWriteIdleSocketLifecycleEvent extends SocketLifecycleEvent {

    public ReadWriteIdleSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

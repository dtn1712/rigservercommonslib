package com.rockitgaming.server.commons.network.netty.core;


import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Object responsible for routing outgoing TCP events, and managing {@link Wire} instances' lifecycle.
 */
public class WireManager {

    private final WireFactory wireFactory;
    private final Map<WireId, Wire> wires;

    public WireManager(WireFactory wireFactory) {
        this.wires = new ConcurrentHashMap<>();
        this.wireFactory = wireFactory;
    }

    /**
     * Creates {@link Wire} instance given a framework agnostic {@link WireConfiguration}.
     *
     * @param configuration Configuration of the wire to be created.
     */
    public void createWire(WireConfiguration configuration) {
        wires.put(configuration.getWireId(), wireFactory.newInstance(configuration));
    }

    /**
     * Get the wire from the wire Id
     *
     * @param wireId wire id
     */
    public Wire getWire(WireId wireId) {
        return wires.get(wireId);
    }

    /**
     * Get all the current wires in the manager
     */
    public List<Wire> getAllWires() {
        return new ArrayList(wires.values());
    }

    /**
     * Starts all wire instances managed by this manager.
     */
    public void startWires() {
        wires.forEach((wireId, wire) -> wire.start());
    }

    /**
     * Stops all wire instances managed by this manager.
     */
    public void stopWires() {
        wires.forEach((wireId, wire) -> wire.stop());
    }

    /**
     * Stop all wires and clear internal state.
     */
    public void clear() {
        stopWires();
        wires.clear();
    }
}

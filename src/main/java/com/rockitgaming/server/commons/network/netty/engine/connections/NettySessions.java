package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SessionPool;
import io.netty.channel.Channel;

public class NettySessions extends SessionPool<Channel> {

    @Override
    public SessionId getSessionId(Channel channel) {
        return SessionId.from(String.format("%s->%s", channel.localAddress(), channel.remoteAddress()));
    }
}

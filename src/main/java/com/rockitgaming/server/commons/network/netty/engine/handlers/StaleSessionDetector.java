package com.rockitgaming.server.commons.network.netty.engine.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.WriteTimeoutException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class StaleSessionDetector extends ChannelInboundHandlerAdapter {

    private final boolean shouldDisconnectOnReaderIdle;

    public StaleSessionDetector(boolean shouldDisconnectOnReaderIdle) {
        this.shouldDisconnectOnReaderIdle = shouldDisconnectOnReaderIdle;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext context, Object event) throws Exception {
        if (event instanceof IdleStateEvent) {
            if (isReaderIdleEvent((IdleStateEvent) event) && shouldDisconnectOnReaderIdle) {
                log.warn("Stale channel detected - tearing down channel {}", context.channel());
                context.close();
            }
        }
        context.fireUserEventTriggered(event);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        if (cause instanceof ReadTimeoutException || cause instanceof WriteTimeoutException) {
            log.warn("Timeout exception detected {} - tearing down channel {}", cause, context.channel());
            context.close();
        }
        context.fireExceptionCaught(cause);
    }

    private boolean isReaderIdleEvent(IdleStateEvent idleEvent) {
        return idleEvent.state() == IdleState.READER_IDLE || idleEvent.state() == IdleState.ALL_IDLE;
    }
}

package com.rockitgaming.server.commons.network.netty.core.domain;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Pool of TCP connections indexed by unique identifier.
 *
 * @param <T> Concrete type of TCP connection. Generally dependent on underlying network framework.
 */
@Slf4j
public abstract class SessionPool<T> {

    private final Map<SessionId, T> sessions = new ConcurrentHashMap<>();

    /**
     * Retrieves active connection for a given {@link SessionId}.
     *
     * @param sessionId Unique identifier for a TCP connection.
     * @return Active TCP connection for a given identifier, if any.
     */
    public Optional<T> getSession(SessionId sessionId) {
        if (sessionId.isAny()) {
            List<T> currentSessions = new ArrayList<>(sessions.values());
            Collections.shuffle(currentSessions);
            return currentSessions.stream().findFirst();
        }
        return Optional.ofNullable(sessions.get(sessionId));
    }

    /**
     * Registers an active TCP connection.
     *
     * @param session Instance representing an active TCP connection.
     */
    public void registerSession(T session) {
        SessionId sessionId = getSessionId(session);
        log.info("Registering active session {}", sessionId);
        sessions.put(sessionId, session);
    }

    /**
     * Unregisters an active TCP connection.
     *
     * @param session Instance representing an active TCP connection.
     */
    public void unregisterSession(T session) {
        SessionId sessionId = getSessionId(session);
        log.info("Unregistering active session {}", sessionId);
        sessions.remove(sessionId);
    }

    /**
     * Generates a unique identifier for an active TCP connection.
     *
     * @param session Instance representing an active TCP connection.
     * @return Unique identifier for an active TCP connection.
     */
    public abstract SessionId getSessionId(T session);
}

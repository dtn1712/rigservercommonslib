package com.rockitgaming.server.commons.network.netty.engine.handlers;


import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import com.rockitgaming.server.commons.network.netty.core.events.ExceptionalSocketEvent;
import com.rockitgaming.server.commons.network.netty.core.events.InboundSocketMessageEvent;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ActiveSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.InactiveSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ReadCompleteSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ReadIdleSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ReadWriteIdleSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.RegisteredSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.UnregisteredSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.WritabilityChangedSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.WriteIdleSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySessions;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WireEventHandler extends ChannelInboundHandlerAdapter {

    private final WireId wireId;
    private final NettySessions sessionPool;
    private final SocketEventHandler eventHandler;

    public WireEventHandler(WireId wireId, NettySessions sessionPool, SocketEventHandler eventHandler) {
        this.sessionPool = sessionPool;
        this.wireId = wireId;
        this.eventHandler = eventHandler;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        log.error(String.format("Handler of wire [%s] caught exception", wireId.getValue()), cause);
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        eventHandler.handle(context, new ExceptionalSocketEvent(wireId, sessionId, cause));
        context.fireExceptionCaught(cause);
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object byteBuf) {
        try {
            ByteBuf buffer = (ByteBuf) byteBuf;
            byte[] data = new byte[buffer.readableBytes()];
            buffer.readBytes(data);

            SessionId sessionId = sessionPool.getSessionId(context.channel());
            SocketMessage message = SocketMessage.from(data);

            log.debug("Received TCP message: {}:{} - {}", wireId, sessionId, message);
            eventHandler.handle(context, new InboundSocketMessageEvent(wireId, sessionId, message));
        } finally {
            ReferenceCountUtil.release(byteBuf);
        }
    }

    @Override
    public void channelRegistered(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        eventHandler.handle(context, new RegisteredSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelRegistered();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        eventHandler.handle(context, new UnregisteredSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelUnregistered();
    }

    @Override
    public void channelActive(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        sessionPool.registerSession(context.channel());
        eventHandler.handle(context, new ActiveSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        sessionPool.unregisterSession(context.channel());
        eventHandler.handle(context, new InactiveSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelInactive();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        eventHandler.handle(context, new ReadCompleteSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelReadComplete();
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext context) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        eventHandler.handle(context, new WritabilityChangedSocketLifecycleEvent(wireId, sessionId));
        context.fireChannelWritabilityChanged();
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext context, Object evt) {
        SessionId sessionId = sessionPool.getSessionId(context.channel());
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleEvent = (IdleStateEvent) evt;
            switch (idleEvent.state()) {
            case ALL_IDLE:
                eventHandler.handle(context, new ReadWriteIdleSocketLifecycleEvent(wireId, sessionId));
                break;
            case READER_IDLE:
                eventHandler.handle(context, new ReadIdleSocketLifecycleEvent(wireId, sessionId));
                break;
            case WRITER_IDLE:
                eventHandler.handle(context, new WriteIdleSocketLifecycleEvent(wireId, sessionId));
                break;
            default:
                break;
            }
        }
        context.fireUserEventTriggered(evt);
    }
}

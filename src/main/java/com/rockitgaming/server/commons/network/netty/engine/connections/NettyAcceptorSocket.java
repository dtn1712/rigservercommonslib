package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketConfiguration;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

final class NettyAcceptorSocket implements NettySocket {

    private static final int DEFAULT_BACKLOG_VALUE = 128;

    private final EventLoopGroup workerLoopGroup;
    private final ServerBootstrap bootstrap;
    private final NettySessions sessions;

    NettyAcceptorSocket(
        ChannelInitializer<SocketChannel> initializer,
        NettySessions sessions,
        SocketConfiguration config)
    {
        this.sessions = sessions;
        this.workerLoopGroup = NioEventLoopGroupFactory.eventLoopGroup(config.getWorkerThreadCount());
        this.bootstrap = createBootstrap(workerLoopGroup, initializer, config);
    }

    private ServerBootstrap createBootstrap(
        EventLoopGroup workerLoopGroup,
        ChannelInitializer<SocketChannel> initializer,
        SocketConfiguration config)
    {
        return new ServerBootstrap()
            .group(workerLoopGroup)
            .channel(NioServerSocketChannel.class)
            .handler(new LoggingHandler(LogLevel.INFO))
            .childHandler(initializer)
            .option(ChannelOption.SO_BACKLOG, DEFAULT_BACKLOG_VALUE)
            .childOption(ChannelOption.SO_KEEPALIVE, config.isKeepAlive())
            .childOption(ChannelOption.TCP_NODELAY, config.isTcpNoDelay())
            .localAddress(config.getAddress());
    }

    @Override
    public void start(ChannelFutureListener onOpenListener) throws Exception {
        bootstrap.bind().addListener(onOpenListener).sync();
    }

    @Override
    public void stop() {
        workerLoopGroup.shutdownGracefully();
    }

    @Override
    public Optional<Channel> getSession(SessionId sessionId) {
        return sessions.getSession(sessionId);
    }

    @Override
    public ScheduledExecutorService getWorkerLoopGroup() {
        return workerLoopGroup;
    }
}

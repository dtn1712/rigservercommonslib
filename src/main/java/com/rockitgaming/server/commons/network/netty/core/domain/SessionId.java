package com.rockitgaming.server.commons.network.netty.core.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Unique identifier for TCP connections.
 */
public final class SessionId {

    public static final SessionId ANY = SessionId.from("ANY");

    private final String value;

    private SessionId(String value) {
        this.value = value;
    }

    public static SessionId from(String value) {
        checkArgument(value != null, "Value cannot be null");
        checkArgument(!value.isEmpty(), "Value cannot be empty");
        return new SessionId(value);
    }

    public String getValue() {
        return value;
    }

    public boolean isAny() {
        return this.equals(SessionId.ANY);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

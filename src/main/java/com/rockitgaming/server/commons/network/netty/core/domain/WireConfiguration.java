package com.rockitgaming.server.commons.network.netty.core.domain;

import com.rockitgaming.server.commons.network.netty.core.codec.CodecConfiguration;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Configuration of TCP wires.
 */
public final class WireConfiguration {

    /**
     * Enumeration representing different types of wires.
     */
    public enum WireType {
        CONNECTOR, ACCEPTOR
    }

    private final WireId wireId;
    private final SocketConfiguration socketConfiguration;
    private final CodecConfiguration codecConfiguration;
    private final WireType type;

    private WireConfiguration(
        WireId wireId,
        WireType type,
        SocketConfiguration socketConfiguration,
        CodecConfiguration codecConfiguration)
    {
        this.wireId = wireId;
        this.type = type;
        this.socketConfiguration = socketConfiguration;
        this.codecConfiguration = codecConfiguration;
    }

    /**
     * Unique identifier for wires.
     *
     * @return Unique wire identifier for current wire configuration.
     */
    public WireId getWireId() {
        return wireId;
    }

    /**
     * Type of wire.
     *
     * @return Type of wire for current wire configuration.
     */
    public WireType getType() {
        return type;
    }

    /**
     * Socket attributes.
     *
     * @return Socket attributes for current wire configuration.
     */
    public SocketConfiguration getSocketConfiguration() {
        return socketConfiguration;
    }

    /**
     * Parameters defining how TCP frame should be parsed.
     *
     * @return Codec parameters for current wire configuration.
     */
    public CodecConfiguration getCodecConfiguration() {
        return codecConfiguration;
    }

    /**
     * Creates configuration for connector wires.
     *
     * @param wireId      Unique identifier for wire.
     * @param hostname    Host used to establish the connection.
     * @param port        Port used to establish the connection.
     * @param codecConfig Codec parameters for current wire configuration.
     * @return New socket configuration for acceptors.
     */
    public static WireConfiguration forConnector(
        WireId wireId,
        String hostname,
        int port,
        CodecConfiguration codecConfig)
    {
        validateParameters(wireId, codecConfig);
        SocketConfiguration socketConfiguration = SocketConfiguration.forConnector(hostname, port);
        return new WireConfiguration(wireId, WireType.CONNECTOR, socketConfiguration, codecConfig);
    }

    /**
     * Creates configuration for connector wires.
     *
     * @param wireId      Unique identifier for wire.
     * @param codecConfig Codec parameters for current wire configuration.
     * @param socketConfig Socket configuration.
     * @return New socket configuration for acceptors.
     */
    public static WireConfiguration forConnector(
            WireId wireId,
            CodecConfiguration codecConfig, SocketConfiguration socketConfig)
    {
        validateParameters(wireId, codecConfig, socketConfig);
        return new WireConfiguration(wireId, WireType.CONNECTOR, socketConfig, codecConfig);
    }

    /**
     * Creates configuration for acceptor wires.
     *
     * @param wireId      Unique identifier for wire.
     * @param port        Port used to listen for incoming connections.
     * @param codecConfig Codec parameters for current wire configuration.
     * @return New socket configuration for acceptors.
     */
    public static WireConfiguration forAcceptor(WireId wireId, int port, CodecConfiguration codecConfig) {
        validateParameters(wireId, codecConfig);
        SocketConfiguration socketConfiguration = SocketConfiguration.forAcceptor(port);
        return new WireConfiguration(wireId, WireType.ACCEPTOR, socketConfiguration, codecConfig);
    }

    /**
     * Creates configuration for acceptor wires.
     *
     * @param wireId      Unique identifier for wire.
     * @param codecConfig Codec parameters for current wire configuration.
     * @param socketConfig Socket configuration.
     * @return New socket configuration for acceptors.
     */
    public static WireConfiguration forAcceptor(WireId wireId, CodecConfiguration codecConfig, SocketConfiguration socketConfig) {
        validateParameters(wireId, codecConfig, socketConfig);
        return new WireConfiguration(wireId, WireType.ACCEPTOR, socketConfig, codecConfig);
    }

    private static void validateParameters(WireId wireId, CodecConfiguration codecConfig) {
        checkArgument(wireId != null, "Wire identification must be non null");
        checkArgument(codecConfig != null, "Codec configuration must be non null");
    }

    private static void validateParameters(WireId wireId, CodecConfiguration codecConfig, SocketConfiguration socketConfig) {
        checkArgument(wireId != null, "Wire identification must be non null");
        checkArgument(codecConfig != null, "Codec configuration must be non null");
        checkArgument(socketConfig != null, "Socket configuration must be non null");
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.google.common.annotations.VisibleForTesting;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class SessionWatchdog {

    private final AtomicBoolean startRequested = new AtomicBoolean(false);
    private final Runnable startSequence;
    private final Duration reconnectInterval;
    private final ScheduledExecutorService executor;

    private Channel session;

    public SessionWatchdog(
        Runnable startSequence,
        Duration reconnectInterval,
        ScheduledExecutorService executor)
    {
        this.startSequence = startSequence;
        this.reconnectInterval = reconnectInterval;
        this.executor = executor;
    }

    public void requestStart() {
        startRequested.set(true);
    }

    public void requestStop() {
        startRequested.set(false);
    }

    public void scheduleRestart() {
        if (startRequested.get()) {
            log.warn("Failed to start. Will try again in {} millis", reconnectInterval.toMillis());
            executor.schedule(startSequence, reconnectInterval.toMillis(), TimeUnit.MILLISECONDS);
        }
    }

    public ChannelFutureListener createOnOpenListener() {
        return channelFuture -> {
            if (channelFuture.isSuccess()) {
                log.info("Channel start operation complete with success", channelFuture.isSuccess());
                Channel channel = channelFuture.channel();
                channel.closeFuture().addListener(createOnCloseListener());
                this.session = channel;
            }
            else {
                log.warn("Channel start operation failed");
            }
        };
    }

    public Channel getSession() {
        return session;
    }

    @VisibleForTesting
    ChannelFutureListener createOnCloseListener() {
        return channelFuture -> {
            log.warn("Channel {} has been closed. Scheduling restart...", channelFuture.channel());
            channelFuture.channel().disconnect();
            scheduleRestart();
        };
    }
}

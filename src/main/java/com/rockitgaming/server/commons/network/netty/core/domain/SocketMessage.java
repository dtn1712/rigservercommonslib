package com.rockitgaming.server.commons.network.netty.core.domain;

import com.google.common.primitives.Bytes;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Object transporting parsed frame data from TCP connection.
 */
public final class SocketMessage {

    private final byte[] data;

    private SocketMessage(byte[] data) {
        this.data = data;
    }

    /**
     * Construct new {@link SocketMessage} from raw byte array with endianness information.
     *
     * @param data Raw socket data.
     * @return {@link SocketMessage} containing raw data from byte array.
     */
    public static SocketMessage from(byte[] data) {
        checkArgument(data != null, "Data cannot be null");
        return new SocketMessage(data);
    }

    /**
     * Construct new {@link SocketMessage} from data of ASCII string object.
     *
     * @param message ASCII string object
     * @return {@link SocketMessage} containing raw data from ASCII string object.
     */
    public static SocketMessage fromAsciiString(String message) {
        checkArgument(message != null, "Message cannot be null");
        return new SocketMessage(message.getBytes(StandardCharsets.US_ASCII));
    }

    /**
     * Construct new {@link SocketMessage} from data of UTF8 string object.
     *
     * @param message UTF8 string object
     * @return {@link SocketMessage} containing raw data from UTF8 string object.
     */
    public static SocketMessage fromUtf8String(String message) {
        checkArgument(message != null, "Message cannot be null");
        return new SocketMessage(message.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * {@link ByteBuffer} containing internal data combined with specified endianness.
     *
     * @param endianness Endianness that should be used to interpret the data.
     * @return ByteBuffer representing data according to specified endianness.
     */
    public ByteBuffer asByteBuffer(ByteOrder endianness) {
        return ByteBuffer.wrap(getData()).order(endianness);
    }

    /**
     * ASCII String representation of internal data.
     *
     * @return ASCII String of data in this message.
     */
    public String asAsciiString() {
        return new String(getData(), StandardCharsets.US_ASCII);
    }

    /**
     * UTF_8 String representation of internal data.
     *
     * @return UTF_8 String of data in this message.
     */
    public String asUtf8String() {
        return new String(getData(), StandardCharsets.UTF_8);
    }

    /**
     * Copy of internal data in this socket message.
     *
     * @return Copy of internal data in this socket message.
     */
    public byte[] getData() {
        return data.clone();
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return Bytes.asList(data).toString();
    }
}

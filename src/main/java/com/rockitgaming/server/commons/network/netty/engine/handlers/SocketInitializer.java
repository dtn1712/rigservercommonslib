package com.rockitgaming.server.commons.network.netty.engine.handlers;

import com.google.common.annotations.VisibleForTesting;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class SocketInitializer extends ChannelInitializer<SocketChannel> {

    @VisibleForTesting
    static final String DECODER = "decoderHandler";

    @VisibleForTesting
    static final String ENCODER = "encoderHandler";

    @VisibleForTesting
    static final String IDLE_STATE = "idleStateHandler";

    @VisibleForTesting
    static final String STALE_SESSION = "staleSessionHandler";

    @VisibleForTesting
    static final String SOCKET_EVENT = "eventHandler";

    private final HandlerFactory handlerFactory;

    public SocketInitializer(HandlerFactory handlerFactory) {
        this.handlerFactory = handlerFactory;
    }

    @Override
    public void initChannel(SocketChannel channel) throws Exception {
        channel.pipeline().addLast(DECODER, handlerFactory.createDecoder());
        channel.pipeline().addLast(ENCODER, handlerFactory.createEncoder());
        channel.pipeline().addLast(IDLE_STATE, handlerFactory.createIdleStateHandler());
        channel.pipeline().addLast(STALE_SESSION, handlerFactory.createStaleSessionDetector());
        channel.pipeline().addLast(SOCKET_EVENT, handlerFactory.createEventHandler());
    }
}

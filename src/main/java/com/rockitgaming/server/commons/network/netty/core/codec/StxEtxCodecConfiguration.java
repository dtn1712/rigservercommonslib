package com.rockitgaming.server.commons.network.netty.core.codec;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Codec configuration for STX ETX delimited frames.
 */
public final class StxEtxCodecConfiguration implements CodecConfiguration {

    public static final byte ETX = 0x03;
    public static final byte STX = 0x02;

    private final byte stx;
    private final byte etx;

    public StxEtxCodecConfiguration(byte stx, byte etx) {
        checkArgument(stx != etx, "STX and ETX must be different");
        this.stx = stx;
        this.etx = etx;
    }

    /**
     * Frame start of text character.
     *
     * @return Frame STX character.
     */
    public byte getStx() {
        return stx;
    }

    /**
     * Frame end of text character.
     *
     * @return Frame ETX character.
     */
    public byte getEtx() {
        return etx;
    }

    /**
     * Codec type represented by this configuration instance.
     *
     * @return Codec type.
     */
    @Override
    public CodecType getType() {
        return CodecType.DELIMITED;
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public static StxEtxCodecConfiguration asciiStxEtx() {
        return new StxEtxCodecConfiguration(STX, ETX);
    }
}

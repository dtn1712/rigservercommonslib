package com.rockitgaming.server.commons.network.netty.engine;

import com.google.common.annotations.VisibleForTesting;
import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import com.rockitgaming.server.commons.network.netty.core.exceptions.SocketMessageRoutingException;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySocket;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySocketFactory;
import com.rockitgaming.server.commons.network.netty.engine.connections.SessionWatchdog;
import com.rockitgaming.server.commons.network.netty.util.OptionalConsumer;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import java.net.SocketAddress;
import java.time.Duration;

@Slf4j
class NettyWire implements Wire {

    private final NettySocket socket;
    private final SessionWatchdog watchdog;
    private final WireConfiguration config;

    NettyWire(NettySocketFactory socketFactory,WireConfiguration config) {
        this.config = config;
        this.socket = socketFactory.newInstance(config);
        this.watchdog = createWatchdog(socket, config);
    }

    @Override
    public WireId getWireId() {
        return config.getWireId();
    }

    @Override
    public void sendMessage(SessionId toSessionId, SocketMessage message) {
        OptionalConsumer.of(socket.getSession(toSessionId))
            .ifPresent(session -> session.writeAndFlush(message))
            .ifNotPresent(() -> throwInvalidSessionId(message, toSessionId));
    }

    @Override
    public void start() {
        watchdog.requestStart();
        try {
            log.debug("Starting wire {}", getWireId());
            SocketAddress address = config.getSocketConfiguration().getAddress();
            socket.start(watchdog.createOnOpenListener());
            log.info("Successfully connected wire {} to {}", getWireId(), address);
        }
        catch (Exception e) {
            log.error(String.format("Error while executing start sequence for wire %s", getWireId()), e);
            watchdog.scheduleRestart();
        }
    }

    @Override
    public void stop() {
        watchdog.requestStop();
        socket.stop();
    }

    @Override
    public Channel getCurrentSession() {
        return watchdog.getSession();
    }

    private void throwInvalidSessionId(SocketMessage message, SessionId toSessionId) {
        String description = String.format("SessionId %s is not present in the session pool", toSessionId);
        throw new SocketMessageRoutingException(message, description);
    }

    @VisibleForTesting
    SessionWatchdog createWatchdog(NettySocket socket, WireConfiguration config) {
        Duration reconnectInterval = config.getSocketConfiguration().getReconnectInterval();
        return new SessionWatchdog(this::start, reconnectInterval, socket.getWorkerLoopGroup());
    }
}

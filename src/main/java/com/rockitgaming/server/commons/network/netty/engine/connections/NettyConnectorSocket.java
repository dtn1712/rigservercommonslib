package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketConfiguration;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

final class NettyConnectorSocket implements NettySocket {

    private final NettySessions sessions;
    private final EventLoopGroup workerLoopGroup;
    private final Bootstrap bootstrap;

    NettyConnectorSocket(
        ChannelInitializer<SocketChannel> initializer,
        NettySessions sessions,
        SocketConfiguration config)
    {
        this.sessions = sessions;
        this.workerLoopGroup = NioEventLoopGroupFactory.eventLoopGroup(config.getWorkerThreadCount());
        this.bootstrap = createBootstrap(workerLoopGroup, initializer, config);
    }

    private Bootstrap createBootstrap(
        EventLoopGroup workerLoopGroup,
        ChannelInitializer<SocketChannel> initializer,
        SocketConfiguration config)
    {
        return new Bootstrap()
            .group(workerLoopGroup)
            .channel(NioSocketChannel.class)
            .handler(initializer)
            .option(ChannelOption.SO_KEEPALIVE, config.isKeepAlive())
            .option(ChannelOption.TCP_NODELAY, config.isTcpNoDelay())
            .remoteAddress(config.getAddress());
    }

    @Override
    public void start(ChannelFutureListener onOpenListener) throws Exception {
        bootstrap.connect().addListener(onOpenListener).sync();
    }

    @Override
    public void stop() {
        workerLoopGroup.shutdownGracefully();
    }

    @Override
    public Optional<Channel> getSession(SessionId sessionId) {
        return sessions.getSession(sessionId);
    }

    @Override
    public ScheduledExecutorService getWorkerLoopGroup() {
        return workerLoopGroup;
    }
}

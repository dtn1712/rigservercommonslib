package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that a socket connection has been unregistered, and cannot be used anymore.
 */
public class UnregisteredSocketLifecycleEvent extends SocketLifecycleEvent {

    public UnregisteredSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that a socket connection is now inactive and reached its end of lifetime.
 */
public class InactiveSocketLifecycleEvent extends SocketLifecycleEvent {

    public InactiveSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

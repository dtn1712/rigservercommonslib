package com.rockitgaming.server.commons.network.netty.engine.codec;

import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class StxEtxBasedFrameDecoder extends ByteToMessageDecoder {

    private final StxEtxCodecConfiguration configuration;

    public StxEtxBasedFrameDecoder(StxEtxCodecConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void decode(ChannelHandlerContext context, ByteBuf in, List<Object> out) throws Exception {
        boolean decodingStarted = false;
        Marker marker = new Marker();

        int readableBytes = in.readableBytes();
        for (int index = 0; index < readableBytes; index++) {
            byte currentByte = in.getByte(index);
            if (currentByte == configuration.getStx()) {
                if (!decodingStarted) {
                    // increment index to skip STX byte
                    marker.setStart(index + 1);
                    decodingStarted = true;
                }
                else {
                    log.warn("Invalid frame with multiple STX delimiters detected");
                }
            }
            if (currentByte == configuration.getEtx()) {
                if (decodingStarted) {
                    decodingStarted = false;
                    // decrement index to skip ETX byte
                    ByteBuf sliced = in.readerIndex(marker.getStart()).readSlice(marker.getSize(index - 1));
                    out.add(sliced.retain());
                    marker.reset();
                }
                else {
                    log.warn("Invalid frame with no STX delimiter detected");
                }
            }
        }
        in.setIndex(0, 0);
    }

    private static class Marker {

        private int start = 0;

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getSize(int end) {
            return Math.max(0, end - start + 1);
        }

        public void reset() {
            setStart(0);
        }
    }
}

package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.google.common.annotations.VisibleForTesting;
import io.netty.channel.nio.NioEventLoopGroup;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

class NioEventLoopGroupFactory {

    static NioEventLoopGroup eventLoopGroup(int nThreads) {
        return new NioEventLoopGroup(nThreads, new NettyThreadFactory());
    }

    @VisibleForTesting
    static class NettyThreadFactory implements ThreadFactory {

        private static final String THREAD_NAME_PREFIX_FORMAT = "netty-pool-%d-thread-%s";
        private static final AtomicInteger POOL_NUMBER = new AtomicInteger(1);

        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final int poolNumber;
        private final ThreadGroup group;

        NettyThreadFactory() {
            SecurityManager manager = System.getSecurityManager();
            group = (manager != null) ? manager.getThreadGroup() : Thread.currentThread().getThreadGroup();
            poolNumber = POOL_NUMBER.getAndIncrement();
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(group, r, getPrefix(), 0);
            if (thread.isDaemon()) {
                thread.setDaemon(false);
            }
            if (thread.getPriority() != Thread.NORM_PRIORITY) {
                thread.setPriority(Thread.NORM_PRIORITY);
            }
            return thread;
        }

        private String getPrefix() {
            return String.format(THREAD_NAME_PREFIX_FORMAT, poolNumber, threadNumber.getAndIncrement());
        }

        @VisibleForTesting
        int getPoolNumber() {
            return poolNumber;
        }
    }
}

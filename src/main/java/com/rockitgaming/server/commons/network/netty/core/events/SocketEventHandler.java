package com.rockitgaming.server.commons.network.netty.core.events;

import io.netty.channel.ChannelHandlerContext;

/**
 * Created by dangn on 6/19/16.
 */
public interface SocketEventHandler {

    void handle(ChannelHandlerContext context, SocketEvent event);
}

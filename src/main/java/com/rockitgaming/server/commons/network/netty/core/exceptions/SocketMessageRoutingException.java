package com.rockitgaming.server.commons.network.netty.core.exceptions;

import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Exception defining exceptional errors while routing TCP messages.
 */
public class SocketMessageRoutingException extends RuntimeException {

    private final SocketMessage message;

    public SocketMessageRoutingException(SocketMessage message, String description) {
        super(description);
        this.message = message;
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

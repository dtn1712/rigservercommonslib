package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that socket connection writability state changed.
 */
public class WritabilityChangedSocketLifecycleEvent extends SocketLifecycleEvent {

    public WritabilityChangedSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

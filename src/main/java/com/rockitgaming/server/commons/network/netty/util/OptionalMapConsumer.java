package com.rockitgaming.server.commons.network.netty.util;

import java.util.Map;
import java.util.function.Consumer;

public final class OptionalMapConsumer<K, V> {

    private final Map<K, V> map;

    private OptionalMapConsumer(Map<K, V> map) {
        this.map = map;
    }

    public static <K, V> OptionalMapConsumer<K, V> of(Map<K, V> map) {
        return new OptionalMapConsumer<>(map);
    }

    public OptionalMapConsumer<K, V> ifPresent(K key, Consumer<V> consumer) {
        if (map.containsKey(key)) {
            consumer.accept(map.get(key));
        }
        return this;
    }

    public OptionalMapConsumer<K, V> ifNotPresent(K key, Runnable runnable) {
        if (!map.containsKey(key)) {
            runnable.run();
        }
        return this;
    }
}

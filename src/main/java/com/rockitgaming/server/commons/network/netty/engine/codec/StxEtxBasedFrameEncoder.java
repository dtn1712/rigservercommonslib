package com.rockitgaming.server.commons.network.netty.engine.codec;


import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
public class StxEtxBasedFrameEncoder extends MessageToByteEncoder<SocketMessage> {

    private final StxEtxCodecConfiguration configuration;

    public StxEtxBasedFrameEncoder(StxEtxCodecConfiguration configuration) {
        super(SocketMessage.class);
        this.configuration = configuration;
    }

    @Override
    public void encode(ChannelHandlerContext context, SocketMessage message, ByteBuf out)
        throws Exception
    {
        checkArgument(message != null, "Message cannot be null");
        log.debug("Sending socket message: {}", message.toString());
        byte[] stx = toArray(configuration.getStx());
        byte[] etx = toArray(configuration.getEtx());
        out.writeBytes(Unpooled.wrappedBuffer(stx, message.getData(), etx));
    }

    private byte[] toArray(byte delimiter) {
        return new byte[] {
            delimiter
        };
    }
}

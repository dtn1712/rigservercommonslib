package com.rockitgaming.server.commons.network.netty.core;


import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;

/**
 * Factory for {@link Wire} abstractions. Depending on the underlying network framework, a new implementation
 * of this interface has to be provided.
 */
public interface WireFactory {

    /**
     * Creates new {@link Wire} given provided {@link WireConfiguration}.
     *
     * @param configuration Configuration used to create a new wire.
     * @return Wire implementation according to network framework used.
     */
    Wire newInstance(WireConfiguration configuration);
}

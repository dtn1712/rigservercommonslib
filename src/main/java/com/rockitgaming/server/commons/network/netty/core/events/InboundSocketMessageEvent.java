package com.rockitgaming.server.commons.network.netty.core.events;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Socket event defining incoming TCP messages.
 */
public final class InboundSocketMessageEvent extends InboundSocketEvent {

    private final SocketMessage message;

    public InboundSocketMessageEvent(WireId fromWireId, SessionId fromSessionId, SocketMessage message) {
        super(fromWireId, fromSessionId);
        this.message = message;
    }

    public SocketMessage getMessage() {
        return message;
    }

}

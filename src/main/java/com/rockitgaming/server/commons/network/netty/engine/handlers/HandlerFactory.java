package com.rockitgaming.server.commons.network.netty.engine.handlers;

import com.rockitgaming.server.commons.network.netty.core.codec.CodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.engine.codec.CodecFactory;
import com.rockitgaming.server.commons.network.netty.engine.codec.StxEtxCodecFactory;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySessions;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOutboundHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.time.temporal.ChronoUnit;

public class HandlerFactory {

    private final SocketEventHandler eventHandler;
    private final NettySessions socketSessions;
    private final WireConfiguration config;

    public HandlerFactory(SocketEventHandler eventHandler, NettySessions sessions, WireConfiguration config) {
        this.eventHandler = eventHandler;
        this.socketSessions = sessions;
        this.config = config;
    }

    ChannelInboundHandler createDecoder() {
        return createCodecFactory().createDecoder();
    }

    ChannelOutboundHandler createEncoder() {
        return createCodecFactory().createEncoder();
    }

    IdleStateHandler createIdleStateHandler() {
        return new IdleStateHandler(
            (int) config.getSocketConfiguration().getReaderIdleTime().get(ChronoUnit.SECONDS),
            (int) config.getSocketConfiguration().getWriterIdleTime().get(ChronoUnit.SECONDS),
            (int) config.getSocketConfiguration().getAllIdleTime().get(ChronoUnit.SECONDS));
    }

    StaleSessionDetector createStaleSessionDetector() {
        return new StaleSessionDetector(config.getSocketConfiguration().isDisconnectOnReaderIdle());
    }

    ChannelInboundHandler createEventHandler() {
        return new WireEventHandler(config.getWireId(), socketSessions, eventHandler);
    }

    private CodecFactory createCodecFactory() {
        CodecConfiguration codecConfiguration = config.getCodecConfiguration();
        switch (codecConfiguration.getType()) {
        case DELIMITED:
            return new StxEtxCodecFactory((StxEtxCodecConfiguration) codecConfiguration);
        default:
            throw new IllegalArgumentException(
                String.format("Codec type %s is not supported", config.getType()));
        }
    }
}

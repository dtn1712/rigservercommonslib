package com.rockitgaming.server.commons.network.netty.engine.connections;


import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.engine.handlers.HandlerFactory;
import com.rockitgaming.server.commons.network.netty.engine.handlers.SocketInitializer;

public class NettySocketFactory {

    private final SocketEventHandler eventHandler;

    public NettySocketFactory(SocketEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public NettySocket newInstance(WireConfiguration config) {
        NettySessions sessions = new NettySessions();
        SocketInitializer initializer = createInitializer(sessions, config);
        switch (config.getType()) {
            case ACCEPTOR:
                return new NettyAcceptorSocket(initializer, sessions, config.getSocketConfiguration());
            case CONNECTOR:
                return new NettyConnectorSocket(initializer, sessions, config.getSocketConfiguration());
            default:
                throw new IllegalArgumentException(
                String.format("Wire type %s is not supported", config.getType()));
        }
    }

    private SocketInitializer createInitializer(NettySessions sessions, WireConfiguration configuration) {
        return new SocketInitializer(new HandlerFactory(eventHandler, sessions, configuration));
    }
}

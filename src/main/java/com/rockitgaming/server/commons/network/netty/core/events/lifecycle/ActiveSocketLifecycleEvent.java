package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that a socket connection has been registered and it is now operational/ready for use.
 */
public class ActiveSocketLifecycleEvent extends SocketLifecycleEvent {

    public ActiveSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

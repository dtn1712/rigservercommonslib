package com.rockitgaming.server.commons.network.netty.engine;


import com.rockitgaming.server.commons.network.netty.core.WireFactory;
import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySocketFactory;

public class NettyWireFactory implements WireFactory {

    private final NettySocketFactory socketFactory;

    public NettyWireFactory(NettySocketFactory socketFactory) {
        this.socketFactory = socketFactory;
    }

    @Override
    public Wire newInstance(WireConfiguration configuration) {
        return new NettyWire(socketFactory,configuration);
    }
}

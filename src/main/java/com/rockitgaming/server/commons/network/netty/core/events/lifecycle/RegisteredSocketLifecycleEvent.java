package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Indicates that a socket connection has been registered, but not yet ready to be used.
 */
public class RegisteredSocketLifecycleEvent extends SocketLifecycleEvent {

    public RegisteredSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

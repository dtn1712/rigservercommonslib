package com.rockitgaming.server.commons.network.netty.core.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.net.InetSocketAddress;
import java.time.Duration;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Configuration parameters for TCP connections.
 */
public final class SocketConfiguration {

    private static final int DEFAULT_WORKER_THREAD_COUNT = 4;
    private static final boolean DEFAULT_KEEP_ALIVE = true;
    private static final boolean DEFAULT_TCP_NO_DELAY = true;
    private static final boolean DEFAULT_DISCONNECT_ON_READER_IDLE = true;
    private static final Duration DEFAULT_RECONNECT_INTERVAL = Duration.ofSeconds(5);
    private static final Duration DEFAULT_READER_IDLE_INTERVAL = Duration.ofSeconds(60 * 5);
    private static final Duration DEFAULT_WRITER_IDLE_INTERVAL = Duration.ofSeconds(60 * 5);
    private static final Duration DEFAULT_READER_WRITER_IDLE_INTERVAL = Duration.ofSeconds(60 * 10);

    private final InetSocketAddress address;

    private int workerThreadCount = DEFAULT_WORKER_THREAD_COUNT;
    private boolean keepAlive = DEFAULT_KEEP_ALIVE;
    private boolean tcpNoDelay = DEFAULT_TCP_NO_DELAY;
    private boolean disconnectOnReaderIdle = DEFAULT_DISCONNECT_ON_READER_IDLE;
    private Duration reconnectInterval = DEFAULT_RECONNECT_INTERVAL;
    private Duration readerIdleTime = DEFAULT_READER_IDLE_INTERVAL;
    private Duration writerIdleTime = DEFAULT_WRITER_IDLE_INTERVAL;
    private Duration allIdleTime = DEFAULT_READER_WRITER_IDLE_INTERVAL;

    private SocketConfiguration(InetSocketAddress address) {
        this.address = address;
    }

    /**
     * Creates configuration for connector sockets.
     *
     * @param hostname Host used to establish the connection.
     * @param port     Port used to establish the connection.
     * @return New socket configuration for connector.
     */
    public static SocketConfiguration forConnector(String hostname, int port) {
        checkArgument(hostname != null, "Hostname must be non null");
        checkArgument(!hostname.isEmpty(), "Hostname must be non empty");
        checkArgument(port > 0, "Port must be a positive number");
        return new SocketConfiguration(new InetSocketAddress(hostname, port));
    }

    /**
     * Creates configuration for acceptor sockets.
     *
     * @param port Port used to listen for incoming connections.
     * @return New socket configuration for acceptors.
     */
    public static SocketConfiguration forAcceptor(int port) {
        checkArgument(port > 0, "Port must be a positive number");
        return new SocketConfiguration(new InetSocketAddress(port));
    }

    /**
     * Gets number of worker threads used to pool messages from TCP connections.
     *
     * @return Number of worker threads to pool TCP messages.
     */
    public int getWorkerThreadCount() {
        return workerThreadCount;
    }

    /**
     * Sets number of worker threads used to pool messages from TCP connections.
     *
     * @param workerThreadCount Number of worker threads to pool TCP messages.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setWorkerThreadCount(int workerThreadCount) {
        checkArgument(workerThreadCount > 0, "Worker thread count must be a positive number");
        this.workerThreadCount = workerThreadCount;
        return this;
    }

    /**
     * Keep alive configuration for current socket configuration.
     *
     * @return Whether or not keep alive should be enabled for the connection.
     */
    public boolean isKeepAlive() {
        return keepAlive;
    }

    /**
     * A TCP keep-alive packet is simply an ACK with the sequence number set to one less than the current
     * sequence number for the connection. A host receiving one of these ACKs responds with an ACK for the
     * current sequence number. Keep-alives can be used to verify that the device at the remote end of a
     * connection is still available.
     *
     * @param keepAlive Whether or not keep alive should be enabled for the connection.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
        return this;
    }

    /**
     * TCP no-delay configuration for current socket configuration.
     *
     * @return {@code true} if Nagle algorithm should be disabled and {@code false} otherwise.
     */
    public boolean isTcpNoDelay() {
        return tcpNoDelay;
    }

    /**
     * Disable the Nagle algorithm. TCP/IP uses an algorithm known as The Nagle Algorithm to coalesce short
     * segments and improve network efficiency.
     *
     * @param tcpNoDelay {@code true} if Nagle algorithm should be disabled and {@code false} otherwise.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setTcpNoDelay(boolean tcpNoDelay) {
        this.tcpNoDelay = tcpNoDelay;
        return this;
    }

    /**
     * Indicates whether or not TCP connections should be torn down in case of reader or reader-writer idle
     * events.
     *
     * @return Whether or not TCP connections should be torn down in case of reader or reader-writer idle
     * events.
     */
    public boolean isDisconnectOnReaderIdle() {
        return disconnectOnReaderIdle;
    }

    /**
     * In order to prevent stale connections, TCP connections can be torn down upon detection of reader or
     * reader-writer idle events. Reader idle timer can be setup by changing the parameter {@link
     * #readerIdleTime} and reader-writer by changing the parameter {@link #allIdleTime}.
     *
     * @param disconnectOnReaderIdle Indicates whether or not TCP connections should be torn down in case of
     *                               reader or reader-writer idle events.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setDisconnectOnReaderIdle(boolean disconnectOnReaderIdle) {
        this.disconnectOnReaderIdle = disconnectOnReaderIdle;
        return this;
    }

    /**
     * Amount of time that should elapse before triggering a reader idle notification.
     *
     * @return Time that should elapse before triggering a reader idle notification.
     */
    public Duration getReaderIdleTime() {
        return readerIdleTime;
    }

    /**
     * Reader idle notifications are triggered if no messages are received after a certain amount of time. By
     * specifying {@link Duration#ZERO}, reader idle verification will be disabled.
     *
     * @param readerIdleTime Amount of time that should elapse before triggering a reader idle notification.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setReaderIdleTime(Duration readerIdleTime) {
        checkArgument(!readerIdleTime.isNegative(), "Duration must be non-negative");
        this.readerIdleTime = readerIdleTime;
        return this;
    }

    /**
     * Amount of time that should elapse before triggering a writer idle notification. By specifying {@link
     * Duration#ZERO}, writer idle verification will be disabled.
     *
     * @return Time that should elapse before triggering a writer idle notification.
     */
    public Duration getWriterIdleTime() {
        return writerIdleTime;
    }

    /**
     * Writer idle notifications are triggered if no messages are sent after a certain amount of time. By
     * specifying {@link Duration#ZERO}, reader-writer idle verification will be disabled.
     *
     * @param writerIdleTime Amount of time that should elapse before triggering a writer idle notification.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setWriterIdleTime(Duration writerIdleTime) {
        checkArgument(!writerIdleTime.isNegative(), "Duration must be non-negative");
        this.writerIdleTime = writerIdleTime;
        return this;
    }

    /**
     * Amount of time that should elapse before triggering read-writer idle notification.
     *
     * @return Time that should elapse before triggering a read-writer idle notification.
     */
    public Duration getAllIdleTime() {
        return allIdleTime;
    }

    /**
     * Read-writer idle notifications are triggered if no messages are received or sent after a certain amount
     * of time.
     *
     * @param allIdleTime Amount of time that should elapse before triggering a read-writer idle
     *                    notification.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setAllIdleTime(Duration allIdleTime) {
        checkArgument(!allIdleTime.isNegative(), "Duration must be non-negative");
        this.allIdleTime = allIdleTime;
        return this;
    }

    /**
     * The time between connection retries are defined as reconnect interval.
     *
     * @return Time between connection retries.
     */
    public Duration getReconnectInterval() {
        return reconnectInterval;
    }

    /**
     * In case of connection failure, the system will try to automatically reconnect (i.e. connect to remote
     * host if connector or bind to a port if acceptor). The time between connection retries are defined as
     * reconnect interval.
     *
     * @param reconnectInterval Time between connection retries.
     * @return Current socket configuration being modified.
     */
    public SocketConfiguration setReconnectInterval(Duration reconnectInterval) {
        checkArgument(!reconnectInterval.isNegative(), "Duration must be a positive");
        this.reconnectInterval = reconnectInterval;
        return this;
    }

    /**
     * Socket address for current socket configuration. Hostname property of socket address may or may not be
     * set depending on the type of socket configuration (connector or acceptor).
     *
     * @return Socket address for current socket configuration.
     */
    public InetSocketAddress getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

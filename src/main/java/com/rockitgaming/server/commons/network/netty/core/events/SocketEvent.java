package com.rockitgaming.server.commons.network.netty.core.events;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Interface for events from and going to TCP sockets.
 */
public interface SocketEvent {

    /**
     * TCP unique wire identification.
     *
     * @return Unique TCP wire identification.
     */
    WireId getWireId();

    /**
     * TCP unique session identification.
     *
     * @return Unique TCP session identification.
     */
    SessionId getSessionId();
}

package com.rockitgaming.server.commons.network.netty.core.domain;

import io.netty.channel.Channel;

/**
 * Abstraction for TCP wires.
 */
public interface Wire {

    /**
     * Unique identifier for the wire.
     *
     * @return Unique identifier for the wire.
     */
    WireId getWireId();

    /**
     * Sends a {@link SocketMessage} using the specified {@link SessionId} on current wire.
     *
     * @param toSessionId Unique identifier of the connection that will be used to send the message.
     * @param message     Message payload.
     */
    void sendMessage(SessionId toSessionId, SocketMessage message);

    /**
     * Starts TCP wire. It can manifests itself into two different behaviors: (1) start accepting new TCP
     * connections as a server or (2) start establishing TCP connection with remote servers.
     */
    void start();

    /**
     * Stops TCP wire. It can manifest itself into two different behaviors: (1) stop accepting new TCP
     * connections or (2) tear down currently establish TCP connection with remote servers.
     */
    void stop();


    /**
     * Get the current channel session registered by this wire
     *
     * @return Current channel session
     */
    Channel getCurrentSession();
}

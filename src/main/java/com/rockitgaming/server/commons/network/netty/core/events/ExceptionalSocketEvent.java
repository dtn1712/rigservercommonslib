package com.rockitgaming.server.commons.network.netty.core.events;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Socket event defining exceptional events.
 */
public final class ExceptionalSocketEvent extends InboundSocketEvent {

    private final Throwable cause;

    public ExceptionalSocketEvent(WireId wireId, SessionId sessionId, Throwable cause) {
        super(wireId, sessionId);
        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}

package com.rockitgaming.server.commons.network.netty.engine.codec;

import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOutboundHandler;

/**
 * Factory for TCP frames codec in Netty.
 */
public interface CodecFactory {

    /**
     * Creates TCP frame decoder according to codec type.
     *
     * @return TCP frame decoder for Netty messages.
     */
    ChannelInboundHandler createDecoder();

    /**
     * Creates TCP frame encoder according to codec type.
     *
     * @return TCP frame encoder for Netty messages.
     */
    ChannelOutboundHandler createEncoder();
}

package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import com.rockitgaming.server.commons.network.netty.core.events.InboundSocketEvent;

/**
 * Socket event defining TCP socket lifecycle events.
 */
public abstract class SocketLifecycleEvent extends InboundSocketEvent {

    public SocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

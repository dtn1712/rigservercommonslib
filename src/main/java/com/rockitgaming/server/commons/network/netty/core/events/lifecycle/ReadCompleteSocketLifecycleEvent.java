package com.rockitgaming.server.commons.network.netty.core.events.lifecycle;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;

/**
 * Triggered when the last message read by the current read operation has been consumed.
 */
public class ReadCompleteSocketLifecycleEvent extends SocketLifecycleEvent {

    public ReadCompleteSocketLifecycleEvent(WireId wireId, SessionId sessionId) {
        super(wireId, sessionId);
    }
}

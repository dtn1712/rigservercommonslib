package com.rockitgaming.server.commons.network.netty.engine.codec;

import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOutboundHandler;

public class StxEtxCodecFactory implements CodecFactory {

    private final StxEtxCodecConfiguration configuration;

    public StxEtxCodecFactory(StxEtxCodecConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public ChannelInboundHandler createDecoder() {
        return new StxEtxBasedFrameDecoder(configuration);
    }

    @Override
    public ChannelOutboundHandler createEncoder() {
        return new StxEtxBasedFrameEncoder(configuration);
    }
}

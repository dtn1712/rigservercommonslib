package com.rockitgaming.server.commons.network.netty.engine.connections;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;

import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Socket abstraction for Netty. Sockets can connect to clients or accept client connections depending on its
 * initial configuration.
 */
public interface NettySocket {

    /**
     * Initiates the socket, meaning that it will try to connect to remote clients or start accepting remote
     * connections.
     *
     * @param onOpenListener Listener that will be triggered upon socket connections.
     * @throws Exception Exception in case of error while binding to a local port or connecting to a remote
     *                   client during wire startup sequence.
     */
    void start(ChannelFutureListener onOpenListener) throws Exception;

    /**
     * Terminates the socket by tearing down all established connections.
     */
    void stop();

    /**
     * Retrieves Netty session identified by {@link SessionId}.
     *
     * @param sessionId Unique identifier of a session.
     * @return Netty session identified by {@link SessionId}.
     */
    Optional<Channel> getSession(SessionId sessionId);

    /**
     * Worker executor service that is used to handle message by this socket.
     *
     * @return Worker executor used by this socket.
     */
    ScheduledExecutorService getWorkerLoopGroup();
}

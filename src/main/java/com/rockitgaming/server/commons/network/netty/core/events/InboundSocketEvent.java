package com.rockitgaming.server.commons.network.netty.core.events;

import com.rockitgaming.server.commons.network.netty.core.domain.SessionId;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Base implementation for socket event coming from TCP layer.
 */
public abstract class InboundSocketEvent implements SocketEvent {

    private final WireId fromWireId;
    private final SessionId fromSessionId;

    public InboundSocketEvent(WireId fromWireId, SessionId fromSessionId) {
        this.fromWireId = fromWireId;
        this.fromSessionId = fromSessionId;
    }

    @Override
    public WireId getWireId() {
        return fromWireId;
    }

    @Override
    public SessionId getSessionId() {
        return fromSessionId;
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

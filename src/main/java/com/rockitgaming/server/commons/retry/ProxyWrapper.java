package com.rockitgaming.server.commons.retry;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * A wrapper class that adds proxy logic to the functionality of the provided implementation.
 * 
 * Restrictions apply:
 * 
 * <p>
 * Accepts only interfaces, as it will provide a proxied functionality layer on top of the interface methods. Calling
 * {@link #wrap()} if an interface wasn't provided will result in {@code IllegalArgumentException}.
 * 
 * <p>
 * All the proxy logic is wrapped only around methods that are part of the provided interface.
 * 
 * @param <I>
 *            the interface around which the proxy functionality layer is added
 */
public final class ProxyWrapper<I> {

    /** The type of interface around whose methods proxy logic will be added. */
    private final Class<I> interfaceType;

    /** The handler to invoke whenever a target method is invoked, which supplies the proxy logic. */
    private final InvocationHandler handler;

    private ProxyWrapper(final Class<I> interfaceType, final InvocationHandler handler) {
        this.interfaceType = interfaceType;
        this.handler = handler;
    }

    /**
     * Type inferring creator of proxy wrapper classes over a provided implementation.
     * 
     * @param interfaceType
     *            the type of the interface that defines the methods that need the proxy layer
     * @param invocationHandler
     *            the {@code InvocationHandler} that defines the proxy functionality
     * @return a {@code ProxyWrapper} implementation that can create {@code I} instances with built-in proxy logic
     */
    public static <I> ProxyWrapper<I> of(
            Class<I> interfaceType, InvocationHandler invocationHandler) {
        return new ProxyWrapper<I>(interfaceType, invocationHandler);
    }

    /**
     * Creates and returns a proxy wrapper from the configurations supplied to this object.
     * 
     * @return the requested proxy enhanced {@code I} instance
     * @throws {@code IllegalArgumentException} if {@link #interfaceType} is not an interface
     */
    @SuppressWarnings("unchecked")
    public I wrap() {
        return (I) Proxy.newProxyInstance(interfaceType.getClassLoader(), new Class[] {interfaceType}, handler);
    }
}

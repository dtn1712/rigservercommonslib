package com.rockitgaming.server.commons.retry;

import com.rockitgaming.server.commons.retry.util.RetryInvocationHandler;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Adds a retry layer to the methods of an interface backed by a provided implementation.
 * <p>
 * Example use:
 *
 * <pre>
 * class FooImpl implements Foo {...}
 * 
 * Foo foo = new FooImpl();
 * Foo retryableFoo = RetryWrapper.builder(Foo.class, getRetryStrategy(3)).build().wrap(foo);
 * </pre>
 *
 * </p>
 * <p>
 * If instead of an interface a class has been provided, calling {@link #wrap(Object)} results in
 * {@code IllegalArgumentException} being thrown.
 * </p>
 * <p>
 * The retry logic is wrapped only around the methods that are part of the provided interface, and not around the
 * possible additional methods of the implementation.
 * </p>
 *
 * @param <I>
 *            the interface around which the retry layer is added
 * @param <CONTEXT>
 *            the type of the context (can be {@link Void}) that is passed to the {@link RetryStrategy}
 */
public final class RetryWrapper<I, CONTEXT> {

    private static final String USE_NO_RETRY_METHOD_MESSAGE =
            "Can't specify null strategy for method. Use #noRetryMethod if this is your desired behavior";

    private static final String CANT_SPECIFY_THE_SAME_METHOD_TWICE_MESSAGE = "Can't specify the same method twice!";

    private static final String CREATE_BUILDER_WITHOUT_INTERFACE_TYPE_MESSAGE =
            "Cannot create builder without interface type";

    private static final String ADD_NULL_METHOD_MESSAGE = "Cannot add null method to builder";

    private static final String ADD_RETRY_METHOD_NOT_PART_OF_CONTRACT_MESSAGE =
            "Can't add retry method for a method that is not part of the contract";

    /** The type of interface around whose methods retry logic will be added. */
    private final Class<I> interfaceType;

    /** The default retry strategy that will be used when one cannot be found. */
    private final RetryStrategy<CONTEXT> retryStrategy;

    /**
     * A {@code Map} from methods that will be retried to the retry strategy to use. If retry strategy is null, it means
     * the method will not be retried.
     */
    private final Map<Method, RetryStrategy<CONTEXT>> retryMethods;

    /**
     * Private constructor for convenience. See {@link #of(Class, RetryStrategy, Map)} instead.
     */
    private RetryWrapper(final Class<I> interfaceType, final RetryStrategy<CONTEXT> retryStrategy,
            final Map<Method, RetryStrategy<CONTEXT>> retryMethods) {
        this.interfaceType = interfaceType;
        this.retryStrategy = retryStrategy;
        this.retryMethods = retryMethods;
    }

    /**
     * Obtains a wrapped implementation with retry logic.
     *
     * @return an instance of {@code I} with retry capabilities
     */
    public I wrap(I interfaceInstance) {
        return ProxyWrapper
                .of(interfaceType, RetryInvocationHandler.of(interfaceInstance, retryStrategy, retryMethods))
                .wrap();
    }

    /**
     * Type inferring creator of {@code Builder} instances.
     *
     * @param interfaceType
     *            the method contract to wrap with retry logic
     * @param retryStrategy
     *            the default retry strategy to apply
     * @return an instance of a {@code Builder}
     */
    public static <I, CONTEXT> Builder<I, CONTEXT> builder(Class<I> interfaceType,
            RetryStrategy<CONTEXT> retryStrategy) {
        return new Builder<I, CONTEXT>(interfaceType, retryStrategy);
    }

    /**
     * Type inferring creator of {@code Builder} instances.
     *
     * @param interfaceType
     *            the method contract to wrap with retry logic
     * @return an instance of a {@code Builder}
     */
    public static <I, CONTEXT> Builder<I, CONTEXT> builder(Class<I> interfaceType) {
        return new Builder<I, CONTEXT>(interfaceType, null);
    }

    /**
     * Type inferring creator of retryable wrapper classes over a provided implementation.
     *
     * @param interfaceType
     *            the type of the interface that defines the methods that need the retry layer
     * @param retryStrategy
     *            the default retry strategy to apply
     * @param retryMethods
     *            a {@code Map} from {@code Method}s that will be retried to the {@code RetryStrategy} to use. If retry
     *            strategy is null, it means the method will not be retried.
     * @return a {@code I} implementation that has built-in retry logic
     */
    private static <I, CONTEXT> RetryWrapper<I, CONTEXT> of(Class<I> interfaceType,
            RetryStrategy<CONTEXT> retryStrategy, Map<Method, RetryStrategy<CONTEXT>> retryMethods) {
        return new RetryWrapper<I, CONTEXT>(interfaceType, retryStrategy, retryMethods);
    }

    /**
     * Builder class for {@code RetryableWrapper} instances.
     *
     * @param <I>
     *            the interface around which the retry layer is added
     * @param <CONTEXT>
     *            the type of the context (can be {@link Void}) for the retry strategies to use
     */
    public static final class Builder<I, CONTEXT> {
        private final Class<I> interfaceType;
        private final Map<Method, RetryStrategy<CONTEXT>> retryMethods;
        private RetryStrategy<CONTEXT> retryStrategy;

        private Builder(Class<I> interfaceType, RetryStrategy<CONTEXT> retryStrategy) {
            checkArgument(interfaceType != null, CREATE_BUILDER_WITHOUT_INTERFACE_TYPE_MESSAGE);

            this.retryStrategy = retryStrategy;
            this.interfaceType = interfaceType;
            this.retryMethods = new HashMap<Method, RetryStrategy<CONTEXT>>();
        }

        /**
         * Adds a default {@code RetryStrategy} that will be used for retrying all methods of the contract.
         *
         * @param retryStrategy
         *            the {@code RetryStrategy} that will be used for all the methods of the contract
         * @return the {@code Builder} instance with the updated state
         */
        public Builder<I, CONTEXT> retry(RetryStrategy<CONTEXT> retryStrategy) {
            this.retryStrategy = retryStrategy;
            return this;
        }

        /**
         * Adds a {@code Method} that wants retry logic under the hood.
         *
         * @param method
         *            the {@code Method} to add retry logic to
         * @param retryStrategy
         *            a non-null {@code RetryStrategy} to use for this {@code Method}. For a non-retryable method, use
         *            {@link #noRetry(Method)}
         * @return the {@code Builder} instance with the updated state
         */
        public Builder<I, CONTEXT> retry(Method method, RetryStrategy<CONTEXT> retryStrategy) {
            return retryMethod(method, retryStrategy, true);
        }

        /**
         * Adds a {@code Method} that needs no retry logic.
         *
         * @param method
         *            the {@code Method} which doesn't need retry logic
         * @return the {@code Builder} instance with the updated state
         */
        public Builder<I, CONTEXT> noRetry(Method method) {
            return retryMethod(method, null, false);
        }

        /**
         * Builds the state from this {@code Builder} into a {@code RetryableWrapper} object.
         *
         * @return the built {@code RetryableWrapper}
         */
        public RetryWrapper<I, CONTEXT> build() {
            return of(interfaceType, retryStrategy, retryMethods);
        }

        private Builder<I, CONTEXT> retryMethod(Method method, RetryStrategy<CONTEXT> retryStrategy,
                boolean checkRetryStrategy) {
            checkArgument(method != null, ADD_NULL_METHOD_MESSAGE);
            checkArgument(Arrays.asList(interfaceType.getMethods()).contains(method),
                    ADD_RETRY_METHOD_NOT_PART_OF_CONTRACT_MESSAGE);
            checkArgument(!retryMethods.containsKey(method), CANT_SPECIFY_THE_SAME_METHOD_TWICE_MESSAGE);
            checkArgument(!checkRetryStrategy || retryStrategy != null, USE_NO_RETRY_METHOD_MESSAGE);

            this.retryMethods.put(method, retryStrategy);
            return this;
        }

        private void checkArgument(boolean expression, String message) {
            if (!expression) {
                throw new IllegalArgumentException(message);
            }
        }
    }
}

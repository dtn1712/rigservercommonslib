package com.rockitgaming.server.commons.retry.strategy;

/**
 * Internal utility class.
 */
final class Preconditions {

    /**
     * Private constructor.
     */
    private Preconditions() {
    }

    /**
     * Ensures the truth of an expression involving parameters of the calling instance. Throws
     * {@link IllegalArgumentException} if the expression is false.
     *
     * @param expression
     *            a boolean expression
     * @param message
     *            the message of the thrown exception
     */
    static void checkArgument(final boolean expression, final String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }
}

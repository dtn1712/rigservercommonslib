package com.rockitgaming.server.commons.retry.util;

import com.rockitgaming.server.commons.retry.RetryStrategy;
import com.rockitgaming.server.commons.retry.RetryingCallable;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * An implementation of the {@code InvocationHandler} interface that adds retry logic to invoked methods.
 * 
 * @param <I>
 *            the interface around which the retry retry layer is added
 * @param <CONTEXT>
 *            the type of the context (can be {@link Void}) for the retry strategies to use.
 */
public final class RetryInvocationHandler<I, CONTEXT> implements InvocationHandler {

    private static final String CREATE_WITH_NULL_INTERFACE_INSTANCE_MESSAGE =
            "Can't create retry invocation handler with null interface instance!";

    /** The instance whose methods need retry logic. */
    private final I interfaceInstance;

    /** The default retry strategy that will be used. */
    private final RetryStrategy<CONTEXT> defaultRetryStrategy;

    /**
     * A {@code Map} from methods that will be retried to the retry strategy to use. If retry strategy is null, it
     * explicitly means the method should not be retried. If map is empty, all methods will be retried with
     * {@link #defaultRetryStrategy}, if specified, and no methods will be retried if {@link #defaultRetryStrategy} is
     * not specified.
     */
    private final Map<Method, RetryStrategy<CONTEXT>> retryMethods;

    /**
     * Private constructor for convenience. See instead {@link #of(Object, RetryStrategy, Map)}.
     */
    private RetryInvocationHandler(final I interfaceInstance, RetryStrategy<CONTEXT> defaultRetryStrategy,
            Map<Method, RetryStrategy<CONTEXT>> retryMethods) {
        this.interfaceInstance = interfaceInstance;
        this.defaultRetryStrategy = defaultRetryStrategy;
        this.retryMethods = Collections.unmodifiableMap(nullToEmtpy(retryMethods));
    }

    /**
     * Type inferring creator of {@code RetryInvocationHandler} instances.
     * 
     * @param interfaceInstance
     *            the instance whose methods need retry logic
     * @param defaultRetryStrategy
     *            the default {@code RetryStrategy} to use when retrying
     * @param retryMethods
     *            a {@code Map} from {@code Method}s to do retry for and the {@code RetryStrategy} to use
     */
    public static <I, CONTEXT> RetryInvocationHandler<I, CONTEXT> of(I interfaceInstance,
            RetryStrategy<CONTEXT> defaultRetryStrategy, Map<Method, RetryStrategy<CONTEXT>> retryMethods) {
        checkArgument(interfaceInstance != null, CREATE_WITH_NULL_INTERFACE_INSTANCE_MESSAGE);

        return new RetryInvocationHandler<I, CONTEXT>(interfaceInstance, defaultRetryStrategy, retryMethods);
    }

    @Override
    public Object invoke(Object invoker, final Method method, final Object[] params) throws Throwable {
        RetryStrategy<CONTEXT> methodRetryStrategy = getMethodRetryStrategy(method);
        if (methodRetryStrategy == null) {
            return invoke(method, params);
        }

        return new RetryingCallable<CONTEXT, Object>(methodRetryStrategy,
                new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return invoke(method, params);
                    }
                }).call();
    }

    /**
     * Finds and returns the appropriate {@code RetryStrategy} to use for a given method.
     * 
     * @param method
     *            the {@code Method} for which to find the appropriate retry strategy
     * @return the matching {@code RetryStrategy} if retryable, and null if not retryable
     */
    private RetryStrategy<CONTEXT> getMethodRetryStrategy(Method method) {
        if (!retryMethods.containsKey(method)) {
            return defaultRetryStrategy;
        }
        return retryMethods.get(method);
    }

    /**
     * Invoke method and safely handle InovationTargetExceptions.
     *
     * @param method
     *            the method to invoke
     * @param params
     *            the parameters to pass to the method
     * @throws {@link Exception} the cause of the invocation target exception if it exists, or RuntimeException if not
     */
    private Object invoke(final Method method, final Object[] params) throws Exception {
        try {
            return method.invoke(interfaceInstance, params);
        } catch (InvocationTargetException ite) {
            final Throwable ex = ite.getCause();
            if (null == ex) {
                throw new RuntimeException(
                        "InvocationTargetException cause was null. Wrapping it in a runtime to prevent potential NPEs.",
                        ite);
            }
            if (ex instanceof Error) {
                throw (Error) ex;
            }
            throw (Exception) ex;
        }
    }

    private static void checkArgument(boolean expression, String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    private static <K, V> Map<K, V> nullToEmtpy(Map<K, V> map) {
        if (map == null) {
            return new HashMap<K, V>();
        }
        return map;
    }
}

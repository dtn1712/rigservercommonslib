package com.rockitgaming.server.commons.retry;

/**
 * Decides whether to retry, and how much to wait until the next retry attempt. The decision is based on the start time
 * of the first attempt, the number of failed attempts made so far, the current exception, and an application-specific
 * context.
 * <p>
 * The retry process is organized as follows:
 *
 * <pre>
 *
 * +---- The first attempt starts immediately, with no involvement from the retry strategy.
 * |
 * |            +--(Exception)--+----------------+
 * |            |               |                |
 * v            v               V                V
 *   Attempt 1 -x--- Attempt 2 -x-- - Attempt 3 -x---- Failure
 * | (normal)   | (first retry) | (second retry) |
 * |            |               |                +-- Ask for the delay, fail because -1 was returned.
 * |            |               |
 * |            |               +-- Ask for the delay until the next attempt; failedAttemptsSoFar is 2.
 * |            |
 * |            +-- Ask for the delay until the next attempt; failedAttemptsSoFar is 1.
 * |
 * +-- Record the time of the first attempt (startTs).
 * </pre>
 *
 * </p>
 * <p>
 * The time of the first attempt, the number of failed attempts made so far, and the current exception usually suffice
 * to decide whether to retry or not, and at what time. However, it is recognized that an additional context might be
 * needed by some implementations to supply the retry strategy with some application-specific information, based on
 * which a more informed retry decision can be made. For example, a framework might compute and supply the current TPS
 * and the system load, so that the retry strategy could consider these factors when establishing the time of the next
 * attempt.
 * </p>
 * <p>
 * To make decisions based on individual components, the following implementations may be used:
 * <ul>
 * <li>{@link com.rockitgaming.server.commons.retry.transiency.TransiencyStrategy} decides based only the exception type;</li>
 * <li>{@link com.rockitgaming.server.commons.retry.sequence.Sequence} decides based on the number of attempts made so far.</li>
 * </ul>
 * </p>
 *
 * @param <CONTEXT>
 *            the type of the application-specific context (can be {@link Void})
 * @author Ovidiu Gheorghies
 */
public interface RetryStrategy<CONTEXT> {
    /**
     * Constant value equal to {@code -1}.
     */
    long STOP = -1L;

    /**
     * @param startTs
     *            The time of the first attempt (milliseconds since Epoch)
     * @param failedAttemptsSoFar
     *            The number of failed attempts made so far
     * @param ex
     *            The exception thrown by the latest attempt
     * @param ctx
     *            An optional application-specific context that might be useful in tuning the wait times
     * @return the wait time until the next attempt (milliseconds), or {@link RetryStrategy#STOP} if retrying is not
     *         desired
     */
    long nextDelayMillis(long startTs, int failedAttemptsSoFar, Exception ex, CONTEXT ctx);
}

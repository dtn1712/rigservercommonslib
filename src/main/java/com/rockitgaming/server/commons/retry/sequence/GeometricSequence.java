package com.rockitgaming.server.commons.retry.sequence;

import com.rockitgaming.server.commons.retry.util.SequenceIndexValidator;

/**
 * Defines a geometric sequence.
 * <p>
 * The term at the index {@code i} is equal to the first term multiplied by the {@code i-1} power of the common ration.
 * </p>
 */
public class GeometricSequence implements Sequence {

    private final double firstTerm;
    private final double commonRatio;

    /**
     * @param firstTerm
     *            The first term of the sequence
     * @param commonRatio
     *            The ratio with which the previous element is multiplied to obtain the current element
     */
    public GeometricSequence(double firstTerm, double commonRatio) {
        this.firstTerm = firstTerm;
        this.commonRatio = commonRatio;
    }

    @Override
    public double get(int index) {
        SequenceIndexValidator.validate(index);

        return firstTerm * Math.pow(commonRatio, index - 1);
    }
}

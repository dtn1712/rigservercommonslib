package com.rockitgaming.server.commons.retry.transiency;

import com.rockitgaming.server.commons.retry.util.ClassList;

import java.util.Collection;
import java.util.Collections;

/**
 * Determines if a particular exception is considered allowed based on a collection of {@link Throwable}.
 */
public class AllowTransiencyStrategy implements TransiencyStrategy {

    private final ClassList<Throwable> allowedExceptionTypes;
    private final ClassList<Throwable> allowedExceptionCauseTypes;

    /**
     * Creates a new {@code TransiencyStrategy} that will use the collection given as parameters to decide if a specific
     * exception is transient.
     * 
     * @param allowedExceptionTypes
     *            exception types that will be used to check the transiency of a specific exception
     */
    public AllowTransiencyStrategy(Collection<Class<? extends Throwable>> allowedExceptionTypes) {
        this(allowedExceptionTypes, Collections.<Class<? extends Throwable>> emptyList());
    }

    /**
     * Creates a new {@code TransiencyStrategy} that will use the collections given as parameters to decide if a
     * specific exception is transient.
     * 
     * @param allowedExceptionTypes
     *            exceptions that will be used to check the transiency of a specific exception
     * @param allowedExceptionCauseTypes
     *            exceptions that will be used to check the transiency of a specific exception's causal chain
     */
    public AllowTransiencyStrategy(Collection<Class<? extends Throwable>> allowedExceptionTypes,
            Collection<Class<? extends Throwable>> allowedExceptionCauseTypes) {
        this.allowedExceptionTypes = new ClassList<Throwable>(allowedExceptionTypes);
        this.allowedExceptionCauseTypes = new ClassList<Throwable>(allowedExceptionCauseTypes);
    }

    @Override
    public boolean isTransient(Exception ex) {
        return isAllowed(ex) || isCausalChainAllowed(ex);
    }

    private boolean isAllowed(Exception ex) {
        return this.allowedExceptionTypes.isInstanceOfAny(ex);
    }

    private boolean isCausalChainAllowed(Exception ex) {
        return this.allowedExceptionCauseTypes.isInstanceOfAny(ClassList.getCausalChain(ex));
    }
}

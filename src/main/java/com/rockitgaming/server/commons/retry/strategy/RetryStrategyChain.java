package com.rockitgaming.server.commons.retry.strategy;

import com.rockitgaming.server.commons.retry.RetryStrategy;

import java.util.Arrays;
import java.util.List;

/**
 * A chain of {@link RetryStrategy}-s. To compute how much to wait until the next retry, every {@link RetryStrategy} in
 * the chain is queried: if none blocks the retry, the delay specified by the last one is returned.
 *
 * @param <CONTEXT>
 *            the type of a user defined context (can be {@link Void}).
 */
public class RetryStrategyChain<CONTEXT> implements RetryStrategy<CONTEXT> {
    private final List<RetryStrategy<CONTEXT>> strategies;

    /**
     * @param strategies
     *            from the chain
     */
    public RetryStrategyChain(RetryStrategy<CONTEXT>... strategies) {
        this.strategies = Arrays.asList(strategies);
    }

    @Override
    public long nextDelayMillis(long startTs, int failedAttemptsSoFar, Exception ex, CONTEXT ctx) {
        long v = RetryStrategy.STOP;

        for (RetryStrategy<CONTEXT> strategy : strategies) {
            v = strategy.nextDelayMillis(startTs, failedAttemptsSoFar, ex, ctx);
            if (v == RetryStrategy.STOP) {
                return v;
            }
        }

        return v;
    }
}

package com.rockitgaming.server.commons.retry.strategy;

import com.rockitgaming.server.commons.retry.RetryStrategy;
import com.rockitgaming.server.commons.retry.sequence.Sequence;

import java.math.BigInteger;

/**
 * A retry strategy that implements {@link RetryStrategy}. To compute how much to wait until the next retry, this checks
 * the timeout.
 *
 * @param <CTX>
 *            the type of a user-defined context (can be {@link Void}).
 */
public class TimeoutRetryStrategy<CTX> implements RetryStrategy<CTX> {

    private final long timeout;
    private final Sequence sequence;

    /**
     * @param timeout
     * @param sequence
     */
    public TimeoutRetryStrategy(long timeout, Sequence sequence) {
        this.timeout = timeout;
        this.sequence = sequence;
    }

    @Override
    public long nextDelayMillis(long startTs, int failedAttemptsSoFar, Exception ex, CTX ctx) {
        long delay = (long) sequence.get(failedAttemptsSoFar);

        // Check that the sequence hasn't reached its end conditions
        // (this doesn't take current time into account).
        if (delay < 0) {
            return RetryStrategy.STOP;
        }

        BigInteger timeAfterRetry = BigInteger.valueOf(System.currentTimeMillis()).add(BigInteger.valueOf(delay));
        BigInteger timeMax = BigInteger.valueOf(startTs).add(BigInteger.valueOf(timeout));

        // Checking if waiting for the next retry would exceed the timeout.
        if (timeAfterRetry.compareTo(timeMax) > 0) {
            return RetryStrategy.STOP;
        }

        return delay;
    }
}

package com.rockitgaming.server.commons.retry.sequence;

import com.rockitgaming.server.commons.retry.util.SequenceIndexValidator;

/**
 * Defines a sequence based on another {@link Sequence}, but with all elements exceeding a given ceiling value set to
 * another reset value.
 * <p>
 * For example, if the base sequence returns {@code 10, 20, 30, 40} etc., this sequence returns {@code 10, 20, -1, -1}
 * etc. when a ceiling value of {@code 20} and a reset value of {@code -1} are used.
 * </p>
 */
public class ValueBoundedSequence implements Sequence {
    private final Sequence baseSequence;

    private final double ceilingValue;
    private final double resetValue;

    /**
     * @param ceilingValue
     *            The value of the sequence element over which the {@code resetValue} is used instead
     * @param resetValue
     *            The value to be used for elements exceeding the {@code ceilingValue}
     * @param baseSequence
     *            The base sequence
     */
    public ValueBoundedSequence(double ceilingValue, double resetValue, Sequence baseSequence) {
        this.baseSequence = baseSequence;
        this.ceilingValue = ceilingValue;
        this.resetValue = resetValue;
    }

    @Override
    public double get(int index) {
        SequenceIndexValidator.validate(index);

        double v = baseSequence.get(index);

        return v > ceilingValue ? resetValue : v;
    }
}

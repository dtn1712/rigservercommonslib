package com.rockitgaming.server.commons.retry.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a collection of {@link Class} with some useful operations.
 *
 * @param <T>
 *            the class type for the collection.
 */
public class ClassList<T> {

    private final List<Class<? extends T>> types = new LinkedList<Class<? extends T>>();

    /**
     * @param types
     */
    public ClassList(Collection<Class<? extends T>> types) {
        this.types.addAll(types);
    }

    /**
     * Checks if any of the current object's registered types is instance of the object given as parameter.
     *
     * @param object
     * @return true or false, depending if any of the current object's registered types is instance of the object given
     *         as parameter
     */
    public boolean isInstanceOfAny(final T object) {
        for (Class<? extends T> type : types) {
            if (type.isInstance(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if any of the current object's registered types is instance of any of the object given as parameter.
     * 
     * @param objects
     *            a {@code Collection} that will be checked with the current object's registered types
     * @return true or false, depending if the current object is instance of any of the objects given as parameter
     */
    public boolean isInstanceOfAny(final Collection<T> objects) {
        for (T object : objects) {
            if (isInstanceOfAny(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a {@code Throwable} cause chain as a list: the first entry in the list will be the actual parameter followed
     * by its cause hierarchy.
     * 
     * @param throwable
     *            the {@code Throwable} to extract causes from
     * @return unmodifiable list containing the cause chain starting with the parameter
     */
    public static List<Throwable> getCausalChain(Throwable throwable) {
        List<Throwable> causes = new ArrayList<Throwable>();
        while (throwable != null) {
            causes.add(throwable);
            throwable = throwable.getCause();
        }
        return Collections.unmodifiableList(causes);
    }
}

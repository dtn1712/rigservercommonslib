package com.rockitgaming.server.commons.retry.strategy;

import com.rockitgaming.server.commons.retry.RetryStrategy;
import com.rockitgaming.server.commons.retry.transiency.TransiencyStrategy;

/**
 * A retry strategy that implements {@link RetryStrategy}. To compute how much to wait until the next retry, this checks
 * if the exception is transient according to the {@link TransiencyStrategy} given as parameter in the constructor.
 *
 * @param <CTX>
 *            the type of a user-defined context (can be {@link Void}).
 */
public class TransiencyRetryStrategy<CTX> implements RetryStrategy<CTX> {

    private final TransiencyStrategy transiencyStrategy;

    /**
     * @param transiencyStrategy
     */
    public TransiencyRetryStrategy(TransiencyStrategy transiencyStrategy) {
        this.transiencyStrategy = transiencyStrategy;
    }

    @Override
    public long nextDelayMillis(long startTs, int failedAttemptsSoFar, Exception ex, CTX ctx) {
        if (transiencyStrategy.isTransient(ex)) {
            return 0;
        }
        return RetryStrategy.STOP;
    }
}

package com.rockitgaming.server.commons.retry.sequence;

import com.rockitgaming.server.commons.retry.util.SequenceIndexValidator;

/**
 * Defines a sequence based on another {@link Sequence}, with the positive values changed according to a randomization
 * factor, as follows:
 *
 * <pre>
 * thisSequence[i] = (1 + random(-factor ... +factor)) * baseSequnce[i]
 * </pre>
 * <p>
 * The randomization factor defines the possible magnitude of change for an element in the base sequence. A value of
 * {@code 0} for the randomization factor means results in no change, while a value close to {@code 1} means a resulting
 * possible value between 0 and nearly double the initial value.
 * <p>
 * For example, for a randomization factor of 0.1, a base value {@code v} is transformed into a value in the interval
 * {@code [v-v*10% , v+v*10%]}, with equal probability.
 * </p>
 */
public class RandomizedPositiveSequence implements Sequence {
    private final Sequence baseSequence;

    private final double randomizationFactor;

    /**
     * @param randomizationFactor
     *            The randomization factor
     * @param baseSequence
     *            The base sequence
     */
    public RandomizedPositiveSequence(double randomizationFactor, Sequence baseSequence) {
        if (randomizationFactor < 0 || randomizationFactor >= 1.0) {
            throw new IllegalArgumentException("Randomization factor must be in [0,1).");
        }

        this.baseSequence = baseSequence;
        this.randomizationFactor = randomizationFactor;
    }

    @Override
    public double get(int index) {
        SequenceIndexValidator.validate(index);

        double v = baseSequence.get(index);

        // We only randomize positive values.
        if (v < 0) {
            return v;
        }

        // No point in calling Math.random() if we don't actually randomize.
        if (randomizationFactor == 0) {
            return v;
        }

        double delta = v * randomizationFactor;
        double min = v - delta;
        double max = v + delta;

        return min + Math.random() * (max - min);
    }
}

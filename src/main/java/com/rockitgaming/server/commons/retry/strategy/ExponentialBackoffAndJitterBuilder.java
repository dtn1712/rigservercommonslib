package com.rockitgaming.server.commons.retry.strategy;

import com.rockitgaming.server.commons.retry.RetryStrategy;
import com.rockitgaming.server.commons.retry.sequence.GeometricSequence;
import com.rockitgaming.server.commons.retry.sequence.LengthBoundedSequence;
import com.rockitgaming.server.commons.retry.sequence.RandomizedPositiveSequence;
import com.rockitgaming.server.commons.retry.sequence.Sequence;
import com.rockitgaming.server.commons.retry.transiency.AllowTransiencyStrategy;
import com.rockitgaming.server.commons.retry.transiency.DenyTransiencyStrategy;

import java.util.Collection;

/**
 * Builds a retry strategy that implements an exponential backoff {@link RetryStrategy}.
 * 
 * This is the equivalent of FullJitter Exponential Backoff Retry strategy from
 * http://www.awsarchitectureblog.com/2015/03/backoff.html
 *
 */
public final class ExponentialBackoffAndJitterBuilder {

    private ExponentialBackoffAndJitterBuilderConfig config = new ExponentialBackoffAndJitterBuilderConfig();

    /**
     * Build a {@link RetryStrategy} with the set properties.
     * 
     * @return a {@link RetryStrategy} with the set properties
     */
    @SuppressWarnings("unchecked")
    public <CONTEXT> RetryStrategy<CONTEXT> build() {
        Preconditions.checkArgument(config.getMaxAttempts() >= 1, "At least one attempt (the first one) must be made.");
        int maximumNumberOfDelaysBetweenAttempts = config.getMaxAttempts() - 1;

        Sequence delaySequence =
                new RandomizedPositiveSequence(
                        config.getRandomizationFactor(),
                        new LengthBoundedSequence(
                                maximumNumberOfDelaysBetweenAttempts,
                                RetryStrategy.STOP,
                                new GeometricSequence(config.getInitialIntervalMillis(),
                                        config.getGeometricRatio())));

        return new RetryStrategyChain<CONTEXT>(
                new TransiencyRetryStrategy<CONTEXT>(
                        new DenyTransiencyStrategy(config.getUnrecoverableThrowables())),
                new TransiencyRetryStrategy<CONTEXT>(
                        new AllowTransiencyStrategy(config.getRecoverableThrowables(),
                                config.getRecoverableCauseThrowables())),
                new TimeoutRetryStrategy<CONTEXT>(
                        config.getMaxWait(), delaySequence));
    }

    /**
     * Returns a {@link ExponentialBackoffAndJitterBuilderConfig} instance that represents the configuration of this
     * builder.
     * 
     * @return a {@link ExponentialBackoffAndJitterBuilderConfig} instance that represents the configuration of this
     *         builder
     */
    public ExponentialBackoffAndJitterBuilderConfig getConfig() {
        return config;
    }

    /**
     * Sets a {@link ExponentialBackoffAndJitterBuilderConfig} instance as the configuration for this builder.
     *
     * @param config
     *            a {@link ExponentialBackoffAndJitterBuilderConfig} instance that represents the configuration of this
     *            builder
     */
    public void setConfig(final ExponentialBackoffAndJitterBuilderConfig config) {
        this.config = config;
    }

    /**
     * Returns the same instance with the provided recoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowables(Class[])
     */
    public ExponentialBackoffAndJitterBuilder retryOn(final Class<? extends Throwable>... recoverableThrowables) {
        config.addRecoverableThrowables(recoverableThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided recoverable {@link Throwable} added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowable(Class)
     */
    public ExponentialBackoffAndJitterBuilder retryOn(final Class<? extends Throwable> recoverableThrowable) {
        config.addRecoverableThrowable(recoverableThrowable);
        return this;
    }

    /**
     * Returns the same instance with the provided recoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowables(Collection)
     */
    public ExponentialBackoffAndJitterBuilder retryOn(
            final Collection<Class<? extends Throwable>> recoverableThrowables) {
        config.addRecoverableThrowables(recoverableThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided cause recoverable {@link Throwable} added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowable(Class)
     */
    public ExponentialBackoffAndJitterBuilder
            retryOnCause(final Class<? extends Throwable> recoverableCauseThrowable) {
        config.addRecoverableCauseThrowable(recoverableCauseThrowable);
        return this;
    }

    /**
     * Returns the same instance with the provided cause recoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowables(Collection)
     */
    public ExponentialBackoffAndJitterBuilder
            retryOnCause(final Collection<Class<? extends Throwable>> recoverableCauseThrowables) {
        config.addRecoverableCauseThrowables(recoverableCauseThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided cause recoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addRecoverableCauseThrowables(Class[])
     */
    public ExponentialBackoffAndJitterBuilder
            retryOnCause(final Class<? extends Throwable>... recoverableCauseThrowables) {
        config.addRecoverableCauseThrowables(recoverableCauseThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided unrecoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addUnrecoverableThrowables(Class[])
     */
    public ExponentialBackoffAndJitterBuilder throwOn(final Class<? extends Throwable>... unrecoverableThrowables) {
        config.addUnrecoverableThrowables(unrecoverableThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided unrecoverable {@link Throwable} added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addUnrecoverableThrowable(Class)
     */
    public ExponentialBackoffAndJitterBuilder throwOn(final Class<? extends Throwable> unrecoverableThrowable) {
        config.addUnrecoverableThrowable(unrecoverableThrowable);
        return this;
    }

    /**
     * Returns the same instance with the provided unrecoverable {@link Throwable}-s added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#addUnrecoverableThrowables(Collection)
     */
    public ExponentialBackoffAndJitterBuilder throwOn(
            final Collection<Class<? extends Throwable>> unrecoverableThrowables) {
        config.addUnrecoverableThrowables(unrecoverableThrowables);
        return this;
    }

    /**
     * Returns the same instance with the provided initial interval added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#setInitialIntervalMillis(double)
     */
    public ExponentialBackoffAndJitterBuilder withInitialIntervalMillis(final double initialIntervalMillis) {
        config.setInitialIntervalMillis(initialIntervalMillis);
        return this;
    }

    /**
     * Returns the same instance with the provided geometric ratio added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#setGeometricRatio(double)
     */
    public ExponentialBackoffAndJitterBuilder withExponentialFactor(final double geometricRatio) {
        config.setGeometricRatio(geometricRatio);
        return this;
    }

    /**
     * Returns the same instance with the provided number of maximum attempts added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#setMaxAttempts(int)
     */
    public ExponentialBackoffAndJitterBuilder withMaxAttempts(final int maxAttempts) {
        config.setMaxAttempts(maxAttempts);
        return this;
    }

    /**
     * Returns the same instance with the provided maximum wait time added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#setMaxWait(long)
     */
    public ExponentialBackoffAndJitterBuilder withMaxElapsedTimeMillis(final long maxWait) {
        config.setMaxWait(maxWait);
        return this;
    }

    /**
     * Returns the same instance with the provided randomization factor added.
     * 
     * @see ExponentialBackoffAndJitterBuilderConfig#setRandomizationFactor(double)
     */
    public ExponentialBackoffAndJitterBuilder withRandomizationFactor(final double randomizationFactor) {
        config.setRandomizationFactor(randomizationFactor);
        return this;
    }
}

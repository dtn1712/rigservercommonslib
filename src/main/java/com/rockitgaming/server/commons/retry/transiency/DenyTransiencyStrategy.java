package com.rockitgaming.server.commons.retry.transiency;

import com.rockitgaming.server.commons.retry.util.ClassList;

import java.util.Collection;

/**
 * Determines if a particular exception is considered denied based on a collection of {@link Throwable}.
 *
 */
public class DenyTransiencyStrategy implements TransiencyStrategy {

    private ClassList<Throwable> classList;

    /**
     * @param deniedExceptionTypes
     */
    public DenyTransiencyStrategy(Collection<Class<? extends Throwable>> deniedExceptionTypes) {
        this.classList = new ClassList<Throwable>(deniedExceptionTypes);
    }

    @Override
    public boolean isTransient(Exception ex) {
        return !this.classList.isInstanceOfAny(ex);
    }
}

package com.rockitgaming.server.commons.retry;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runs the underlining {@link Callable} based on the {@link RetryStrategy}.
 * <p>
 * If you wish to use this class to retry a {@link Runnable}, you can use {@link Executors#callable(Runnable)}.
 * </p>
 *
 * @param <T>
 *            the return type of the callable to be retried
 * @param <CONTEXT>
 *            the type of the context that is made available to the retry strategy (can be {@link Void}).
 */
public class RetryingCallable<CONTEXT, T> implements Callable<T> {

    private static final Logger LOG = Logger.getLogger(RetryingCallable.class.getName());

    private final Callable<T> callable;

    private final RetryStrategy<CONTEXT> retryStrategy;

    private final CONTEXT ctx;

    /**
     * Same as {@link #RetryingCallable(RetryStrategy, Object, Callable)}, but with null context.
     *
     * @param callable
     *            the {@link Callable} to be called.
     * @param retryStrategy
     *            the {@link RetryStrategy} to be used.
     */
    public RetryingCallable(RetryStrategy<CONTEXT> retryStrategy, Callable<T> callable) {
        this(retryStrategy, null, callable);
    }

    /**
     * @param retryStrategy
     *            the {@link RetryStrategy} to be used.
     * @param ctx
     *            the custom context to be made available to the retry strategy.
     * @param callable
     *            the {@link Callable} to be called.
     */
    public RetryingCallable(RetryStrategy<CONTEXT> retryStrategy, CONTEXT ctx, Callable<T> callable) {
        this.callable = callable;
        this.retryStrategy = retryStrategy;
        this.ctx = ctx;
    }

    /**
     * @return computed result from delegate {@link #callable}, employing the {@link RetryStrategy} if necessary.
     * @throws Exception
     *             the exception thrown by the delegate runnable at the last failed run attempt.
     */
    @Override
    public T call() throws Exception {
        int failedAttemptsSoFar = 0;
        long startTs = System.currentTimeMillis();
        while (true) {
            try {
                return callable.call();
            } catch (Exception ex) {
                failedAttemptsSoFar++;
                long delay = retryStrategy.nextDelayMillis(startTs, failedAttemptsSoFar, ex, ctx);

                if (delay < 0) {
                    throw ex;
                } else {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE,
                                String.format(
                                        "Retriable exception detected for %s , will retry in %s ms, attempt number: %s",
                                        callable, delay, failedAttemptsSoFar),
                                ex);
                    }
                    Thread.sleep(delay);
                }
            }
        }
    }
}

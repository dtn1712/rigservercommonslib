package com.rockitgaming.server.commons.retry.sequence;

/**
 * Defines an sequence of {@code double}-s, indexed from {@code 1}.
 *
 * @author Ovidiu Gheorghies
 */
public interface Sequence {
    /**
     * Returns the element with index {@code i} in the sequence, starting from index {@code 1}.
     * <p>
     * Implementations MUST throw {@link IllegalArgumentException} for indices strictly lower than {@code 1}.
     * </p>
     *
     * @param index
     *            An index in the sequence (must be greater or equal to 1).
     * @return the value of the sequence element at the given index
     */
    double get(int index);
}

package com.rockitgaming.server.commons.retry.strategy;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * This class is used to set the configuration of a {@link ExponentialBackoffAndJitterBuilder}.
 */
public final class ExponentialBackoffAndJitterBuilderConfig {

    private double initialIntervalMillis = 50;
    private double geometricRatio = 2;
    private double randomizationFactor = Math.nextAfter(1.0, 0); // Just slightly less than 1.0.
    private int maxAttempts = 3;
    private long maxWait = Long.MAX_VALUE;

    private Collection<Class<? extends Throwable>> recoverableThrowables =
            new LinkedList<Class<? extends Throwable>>();

    private Collection<Class<? extends Throwable>> recoverableCauseThrowables =
            new LinkedList<Class<? extends Throwable>>();

    private Collection<Class<? extends Throwable>> unrecoverableThrowables =
            new LinkedList<Class<? extends Throwable>>();

    /**
     * Returns the initial interval in milliseconds.
     * 
     * @return the initial interval in milliseconds
     */
    public double getInitialIntervalMillis() {
        return initialIntervalMillis;
    }

    /**
     * Sets the initial interval in milliseconds. The {@link com.amazon.coral.retry.RetryStrategy} will use this value
     * to wait before the first retry and to compute the next waiting time for the following retries.
     * 
     * @param initialIntervalMillis
     *            the initial interval in milliseconds
     */
    public void setInitialIntervalMillis(double initialIntervalMillis) {
        this.initialIntervalMillis = initialIntervalMillis;
    }

    /**
     * Returns the geometric ratio.
     * 
     * @return the geometric ratio
     */
    public double getGeometricRatio() {
        return geometricRatio;
    }

    /**
     * Sets the geometric ratio. The {@link com.amazon.coral.retry.RetryStrategy} will use this value to compute the
     * wait time values between the retries.
     * 
     * @param geometricRatio
     *            the value for the geometric ratio
     */
    public void setGeometricRatio(double geometricRatio) {
        this.geometricRatio = geometricRatio;
    }

    /**
     * Returns the randomization factor.
     * 
     * @return the randomization factor
     */
    public double getRandomizationFactor() {
        return randomizationFactor;
    }

    /**
     * Sets the randomization factor. The {@link com.amazon.coral.retry.RetryStrategy} will use this value to compute
     * the wait time values between the retries.
     * 
     * The randomization factor is value in the interval [0, 1) which controls how much jitter is applied to the waiting
     * times series. A value of {@code 0} means no randomization, a value close to {@code 1} means a resulting value
     * from {@code 0} up to {@code 200%} of the initial value.
     * 
     * @param randomizationFactor
     *            the value for the randomization factor
     */
    public void setRandomizationFactor(double randomizationFactor) {
        this.randomizationFactor = randomizationFactor;
    }

    /**
     * Returns the maximum number of attempts.
     * 
     * @return the maximum number of attempts
     */
    public int getMaxAttempts() {
        return maxAttempts;
    }

    /**
     * Sets the maximum number of attempts.
     * 
     * @param maxAttempts
     *            the maximum number of attempts (must be at least 1)
     */
    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    /**
     * Returns the maximum time in milliseconds for waiting
     * 
     * @return maximum time for waiting
     */
    public long getMaxWait() {
        return maxWait;
    }

    /**
     * Sets the maximum time in milliseconds f for waiting.
     * 
     * @param maxWait
     *            the maximum time for waiting
     */
    public void setMaxWait(long maxWait) {
        this.maxWait = maxWait;
    }

    /**
     * Marks a {@link Class} of a {@link Throwable} as recoverable.
     * 
     * @param recoverableThrowable
     *            a {@link Class} of a recoverable {@link Throwable}
     */
    public void addRecoverableThrowable(final Class<? extends Throwable> recoverableThrowable) {
        this.recoverableThrowables.add(recoverableThrowable);
    }

    /**
     * Marks an array of {@link Class} instances as recoverable.
     * 
     * @param recoverableThrowables
     *            an array of {@link Class} instances that will be marked as recoverable
     */
    public void addRecoverableThrowables(final Class<? extends Throwable>[] recoverableThrowables) {
        this.recoverableThrowables.addAll(Arrays.asList(recoverableThrowables));
    }

    /**
     * Marks a {@link Collection} of {@link Class} instances as recoverable.
     * 
     * @param recoverableThrowables
     *            a {@link Collection} of {@link Class} instances that will be marked as recoverable
     */
    public void addRecoverableThrowables(final Collection<Class<? extends Throwable>> recoverableThrowables) {
        this.recoverableThrowables.addAll(recoverableThrowables);
    }

    /**
     * Sets a {@link Collection} of {@link Class} instances as recoverable.
     * 
     * @param recoverableThrowables
     *            a {@link Collection} of {@link Class} instances that will be marked as recoverable
     */
    public void setRecoverableThrowables(final Collection<Class<? extends Throwable>> recoverableThrowables) {
        this.recoverableThrowables = recoverableThrowables;
    }

    /**
     * Returns a {@link Collection} of {@link Class} instances with recoverable {@link Throwable}-s.
     * 
     * @return a {@link Collection} of {@link Class} instances that are recoverable {@link Throwable}-s.
     */
    public Collection<Class<? extends Throwable>> getRecoverableThrowables() {
        return Collections.unmodifiableCollection(recoverableThrowables);
    }

    /**
     * Marks a {@link Class} of a {@link Throwable} as cause recoverable.
     * 
     * @param recoverableCauseThrowable
     *            a {@link Class} of a cause recoverable {@link Throwable}
     */
    public void addRecoverableCauseThrowable(Class<? extends Throwable> recoverableCauseThrowable) {
        this.recoverableCauseThrowables.add(recoverableCauseThrowable);
    }

    /**
     * Marks a {@link Collection} of {@link Class} instances as cause recoverable.
     * 
     * @param recoverableCauseThrowables
     *            an array of {@link Class} instances that will be marked as cause recoverable
     */
    public void addRecoverableCauseThrowables(final Collection<Class<? extends Throwable>> recoverableCauseThrowables) {
        this.recoverableCauseThrowables.addAll(recoverableCauseThrowables);
    }

    /**
     * Marks an array of {@link Class} instances as cause recoverable.
     * 
     * @param recoverableCauseThrowables
     *            an array of {@link Class} instances that will be marked as cause recoverable
     */
    public void addRecoverableCauseThrowables(final Class<? extends Throwable>[] recoverableCauseThrowables) {
        this.recoverableCauseThrowables.addAll(Arrays.asList(recoverableCauseThrowables));
    }

    /**
     * Sets a {@link Collection} of {@link Class} instances as cause recoverable.
     * 
     * @param recoverableCauseThrowables
     *            a {@link Collection} of {@link Class} instances that will be marked as cause recoverable
     */
    public void setRecoverableCauseThrowables(final Collection<Class<? extends Throwable>> recoverableCauseThrowables) {
        this.recoverableCauseThrowables = recoverableCauseThrowables;
    }

    /**
     * Returns a {@link Collection} of {@link Class} instances with cause recoverable {@link Throwable}-s.
     * 
     * @return a {@link Collection} of {@link Class} instances with cause recoverable {@link Throwable}-s
     */
    public Collection<Class<? extends Throwable>> getRecoverableCauseThrowables() {
        return Collections.unmodifiableCollection(recoverableCauseThrowables);
    }

    /**
     * Marks a {@link Class} of a {@link Throwable} as unrecoverable {@link Throwable}.
     * 
     * @param unrecoverableThrowable
     *            a {@link Class} of an unrecoverable {@link Throwable}
     */
    public void addUnrecoverableThrowable(final Class<? extends Throwable> unrecoverableThrowable) {
        this.unrecoverableThrowables.add(unrecoverableThrowable);
    }

    /**
     * Marks an array of {@link Class} instances as unrecoverable {@link Throwable}-s.
     * 
     * @param unrecoverableThrowables
     *            an array of {@link Class} instances of unrecoverable {@link Throwable}
     */
    public void addUnrecoverableThrowables(final Class<? extends Throwable>[] unrecoverableThrowables) {
        this.unrecoverableThrowables.addAll(Arrays.asList(unrecoverableThrowables));
    }

    /**
     * Marks a {@link Collection} of {@link Class} instances as unrecoverable {@link Throwable}-s.
     * 
     * @param unrecoverableThrowables
     *            a {@link Class} of an unrecoverable {@link Throwable}
     */
    public void addUnrecoverableThrowables(final Collection<Class<? extends Throwable>> unrecoverableThrowables) {
        this.unrecoverableThrowables.addAll(unrecoverableThrowables);
    }

    /**
     * Sets a {@link Collection} of {@link Class} instances as unrecoverable.
     * 
     * @param unrecoverableThrowables
     *            a {@link Collection} of {@link Class} instances that will be marked as unrecoverable
     */
    public void setUnrecoverableThrowables(final Collection<Class<? extends Throwable>> unrecoverableThrowables) {
        this.unrecoverableThrowables = unrecoverableThrowables;
    }

    /**
     * Returns a {@link Collection} of {@link Class} instances with unrecoverable {@link Throwable}-s.
     * 
     * @return a {@link Collection} of {@link Class} instances with unrecoverable {@link Throwable}-s
     */
    public Collection<Class<? extends Throwable>> getUnrecoverableThrowables() {
        return Collections.unmodifiableCollection(unrecoverableThrowables);
    }
}

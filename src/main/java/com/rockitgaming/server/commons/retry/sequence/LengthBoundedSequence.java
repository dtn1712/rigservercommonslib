package com.rockitgaming.server.commons.retry.sequence;

import com.rockitgaming.server.commons.retry.util.SequenceIndexValidator;

/**
 * Defines a sequence identical to a base {@link Sequence} up to and including a given cutoff index, after which all
 * elements have another specified value.
 * <p>
 * For example, if the base sequence returns {@code 1, 2, 3, 4} etc., this sequence will return {@code 1, 2, 0, 0} etc.
 * when the cutoff index is {@code 1} and the cutoff value is {@code 0}.
 * </p>
 */
public class LengthBoundedSequence implements Sequence {
    private final Sequence baseSequence;

    private final int cutoffIndex;
    private final double cutoffValue;

    /**
     * @param cutoffIndex
     *            The index after which the {@code cutoffValue} is returned, instead of the corresponding value in the
     *            delegate {@link Sequence}.
     * @param cutoffValue
     *            The value of the elements of this sequence, starting from index {@code cutoffIndex + 1}.
     * @param baseSequence
     *            The base sequence that gives the values of elements with indices up to and including cutoffIndex.
     */
    public LengthBoundedSequence(int cutoffIndex, double cutoffValue, Sequence baseSequence) {
        this.baseSequence = baseSequence;
        this.cutoffIndex = cutoffIndex;
        this.cutoffValue = cutoffValue;
    }

    @Override
    public double get(int index) {
        SequenceIndexValidator.validate(index);

        if (index > cutoffIndex) {
            return cutoffValue;
        }
        return baseSequence.get(index);
    }

}

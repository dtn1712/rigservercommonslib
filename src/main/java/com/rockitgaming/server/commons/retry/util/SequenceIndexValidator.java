package com.rockitgaming.server.commons.retry.util;

public final class SequenceIndexValidator {
    private SequenceIndexValidator() {
    }

    public static final void validate(int index) {
        if (index < 1) {
            throw new IllegalArgumentException(
                    String.format("Sequences are indexed from 1, index '%d' given.", index));
        }
    }
}

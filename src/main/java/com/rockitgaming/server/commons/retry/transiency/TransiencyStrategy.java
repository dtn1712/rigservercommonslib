package com.rockitgaming.server.commons.retry.transiency;

/**
 * Determines if a particular exception is to be considered transient.
 * <p>
 * A transient exception is an exception that may not happen again if the operation that caused it is retried.
 * </p>
 *
 * @author Ovidiu Gheorghies
 */
public interface TransiencyStrategy {
    /**
     * @param ex
     * @return true or false, depending if the exception is transient
     */
    boolean isTransient(Exception ex);
}

package com.rockitgaming.server.commons.example.netty;

import com.rockitgaming.server.commons.network.netty.core.WireFactory;
import com.rockitgaming.server.commons.network.netty.core.codec.CodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketMessage;
import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.engine.NettyWireFactory;
import com.rockitgaming.server.commons.network.netty.engine.codec.StxEtxBasedFrameEncoder;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySocketFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

/**
 *
 * This is the chat client. Currently, client which is inactive for amount of time will be
 * disconnected by server to prevent a lot of stale/idle session. The configuration is in
 * SocketConfiguration class
 *
 * Created by dangn on 6/17/16.
 */
public class NettyChatClient {

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 8001;

    public static void main(String[] args) throws Exception {

        // Instantiate client event handler and socket factory
        SocketEventHandler eventHandler = new NettyChatClientEventHandler();
        NettySocketFactory socketFactory = new NettySocketFactory(eventHandler);

        WireId wireId = WireId.from(UUID.randomUUID().toString());

        // Connector is for client
        CodecConfiguration codecConfiguration = StxEtxCodecConfiguration.asciiStxEtx();
        SocketConfiguration socketConfiguration = SocketConfiguration.forConnector(HOST,PORT);
        WireConfiguration wireConfiguration = WireConfiguration.forConnector(wireId,codecConfiguration,socketConfiguration);

        // Create and start wire protocol
        WireFactory nettyWireFactory = new NettyWireFactory(socketFactory);
        Wire wire = nettyWireFactory.newInstance(wireConfiguration);
        wire.start();

        // Enter input from console
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[32 * 1024];
        int bytesRead;
        try {
            while ((bytesRead = System.in.read(buffer)) > 0) {

                // Retrieve message from the console
                baos.write(buffer, 0, bytesRead);
                SocketMessage socketMessage = SocketMessage.from(baos.toByteArray());

                // Retrieve the current active channel session in the wire client
                Channel channel = wire.getCurrentSession();

                // Message is encoded to frame
                StxEtxBasedFrameEncoder encoder = new StxEtxBasedFrameEncoder(StxEtxCodecConfiguration.asciiStxEtx());
                ByteBuf byteBuf = channel.alloc().buffer();
                encoder.encode(null,socketMessage,byteBuf);

                // Send message to server
                channel.writeAndFlush(byteBuf);

                baos.reset();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

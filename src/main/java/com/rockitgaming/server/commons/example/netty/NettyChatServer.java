package com.rockitgaming.server.commons.example.netty;

import com.rockitgaming.server.commons.network.netty.core.WireFactory;
import com.rockitgaming.server.commons.network.netty.core.codec.CodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.codec.StxEtxCodecConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.SocketConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.Wire;
import com.rockitgaming.server.commons.network.netty.core.domain.WireConfiguration;
import com.rockitgaming.server.commons.network.netty.core.domain.WireId;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.engine.NettyWireFactory;
import com.rockitgaming.server.commons.network.netty.engine.connections.NettySocketFactory;

import java.util.UUID;

/**
 *
 * This is the chat server.
 *
 * Created by dangn on 6/9/16.
 */
public class NettyChatServer {

    private static final int PORT = 8001;

    public static void main(String[] args) throws Exception {

        // Instantiate server event handler and socket factory
        SocketEventHandler eventHandler = new NettyChatServerEventHandler();
        NettySocketFactory socketFactory = new NettySocketFactory(eventHandler);

        WireId wireId = WireId.from(UUID.randomUUID().toString());

        // Acceptor is for server
        CodecConfiguration codecConfiguration = StxEtxCodecConfiguration.asciiStxEtx();
        SocketConfiguration socketConfiguration = SocketConfiguration.forAcceptor(PORT);
        WireConfiguration wireConfiguration = WireConfiguration.forAcceptor(wireId,codecConfiguration,socketConfiguration);

        // Create and start wire protocol
        WireFactory nettyWireFactory = new NettyWireFactory(socketFactory);
        Wire wire = nettyWireFactory.newInstance(wireConfiguration);
        wire.start();

    }
}

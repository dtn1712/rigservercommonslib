package com.rockitgaming.server.commons.example.cache;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnectionException;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;

/**
 * Example class to run redis cache. Please install and run redis server
 * first before run this class. Redis can be download and installed here.
 * Linux/Mac: http://redis.io/download
 * Window: https://github.com/MSOpenTech/redis/releases
 */
public class RedisStringCache {

    public static void main(String[] args) {

        try {
            // Syntax: redis://[password@]host[:port][/databaseNumber]
            RedisClient redisClient = RedisClient.create("redis://@localhost:6379/0");
            StatefulRedisConnection<String, String> connection = redisClient.connect();
            RedisCommands<String, String> syncCommands = connection.sync();

            System.out.println("Connected to Redis. Set key hello to value world");

            syncCommands.set("hello","world");

            System.out.println("Key hello is: " + syncCommands.get("hello"));

            System.out.println("Removing key hello...");

            syncCommands.del("hello");

            System.out.println("Key hello is: " + syncCommands.get("hello"));

            connection.close();
            redisClient.shutdown();
        } catch (RedisConnectionException e) {
            System.out.println("Cannot connect to redis server. Please ensure the server is ready");
        }

    }
}

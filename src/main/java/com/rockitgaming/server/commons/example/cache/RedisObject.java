package com.rockitgaming.server.commons.example.cache;

/**
 * Redis Object. Need to have getter, setter and default constructor
 */
public class RedisObject {

    private String stringVal;
    private int intVal;
    private boolean booleanVal;

    public RedisObject() {}

    public RedisObject(String stringVal, boolean booleanVal, int intVal) {
        this.stringVal = stringVal;
        this.booleanVal = booleanVal;
        this.intVal = intVal;
    }

    public int getIntVal() {
        return intVal;
    }

    public void setIntVal(int intVal) {
        this.intVal = intVal;
    }

    public String getStringVal() {
        return stringVal;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public boolean isBooleanVal() {
        return booleanVal;
    }

    public void setBooleanVal(boolean booleanVal) {
        this.booleanVal = booleanVal;
    }
}

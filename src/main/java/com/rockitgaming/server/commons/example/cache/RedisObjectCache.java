package com.rockitgaming.server.commons.example.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnectionException;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;

import java.io.IOException;

/**
 * Example class to run redis cache. Please install and run redis server
 * first before run this class. Redis can be download and installed here.
 * Linux/Mac: http://redis.io/download
 * Window: https://github.com/MSOpenTech/redis/releases
 */
public class RedisObjectCache {

    public static void main(String[] args) {

        try {
            // Syntax: redis://[password@]host[:port][/databaseNumber]
            RedisClient redisClient = RedisClient.create("redis://@localhost:6379/0");
            StatefulRedisConnection<String, String> connection = redisClient.connect();
            RedisCommands<String, String> syncCommands = connection.sync();

            System.out.println("Connected to Redis. Set key object to redis object");

            RedisObject object = new RedisObject("test",true,1);
            ObjectMapper mapper = new ObjectMapper();

            String objectData = mapper.writeValueAsString(object);

            syncCommands.set("object",objectData);

            String redisData = syncCommands.get("object");
            RedisObject redisObject = mapper.readValue(redisData,RedisObject.class);

            System.out.println("Redis object have value: " + redisObject.getStringVal() + "," + redisObject.getIntVal() + "," + redisObject.isBooleanVal());

            connection.close();
            redisClient.shutdown();
        } catch (RedisConnectionException e) {
            System.out.println("Cannot connect to redis server. Please ensure the server is ready");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

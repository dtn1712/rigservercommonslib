package com.rockitgaming.server.commons.example.netty;

import com.rockitgaming.server.commons.network.netty.core.events.ExceptionalSocketEvent;
import com.rockitgaming.server.commons.network.netty.core.events.InboundSocketMessageEvent;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEvent;
import com.rockitgaming.server.commons.network.netty.core.events.SocketEventHandler;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ActiveSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.InactiveSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.ReadCompleteSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.RegisteredSocketLifecycleEvent;
import com.rockitgaming.server.commons.network.netty.core.events.lifecycle.UnregisteredSocketLifecycleEvent;
import io.netty.channel.ChannelHandlerContext;

/**
 *
 * Chat client main logic to handle coming socket event
 *
 * Created by dangn on 6/15/16.
 */
public class NettyChatClientEventHandler implements SocketEventHandler {

    @Override
    public void handle(ChannelHandlerContext context, SocketEvent event) {
        if (event instanceof RegisteredSocketLifecycleEvent) {
            handleRegisteredEvent(context,event);
        } else if (event instanceof ActiveSocketLifecycleEvent) {
            handleActiveEvent(context,event);
        } else if (event instanceof InboundSocketMessageEvent) {
            handleInboundMessageEvent(context,event);
        } else if (event instanceof ReadCompleteSocketLifecycleEvent) {
            handleReadCompleteEvent(context,event);
        } else if (event instanceof InactiveSocketLifecycleEvent) {
            handleInactiveEvent(context,event);
        } else if (event instanceof UnregisteredSocketLifecycleEvent) {
            handleUnregisterEvent(context,event);
        } else if (event instanceof ExceptionalSocketEvent) {
            handleExceptionalEvent(context,event);
        }
    }

    /**
     * Handle event when client channel register with server
     * @param context
     * @param event
     */
    private void handleRegisteredEvent(ChannelHandlerContext context, SocketEvent event) {}

    /**
     * Handle event when client connect with server successfully and actively waiting for socket message
     * @param context
     * @param event
     */
    private void handleActiveEvent(ChannelHandlerContext context, SocketEvent event) {}

    /**
     * Handle event when the message coming
     * @param context
     * @param event
     */
    private void handleInboundMessageEvent(ChannelHandlerContext context, SocketEvent event) {
        InboundSocketMessageEvent messageEvent = (InboundSocketMessageEvent) event;
        System.out.println(messageEvent.getMessage().asAsciiString());
    }

    /**
     * Handle event when completing the inbound message event
     * @param context
     * @param event
     */
    private void handleReadCompleteEvent(ChannelHandlerContext context, SocketEvent event) {}

    /**
     * Handle event when the session is inactive and does not send message for a long time
     * @param context
     * @param event
     */
    private void handleInactiveEvent(ChannelHandlerContext context, SocketEvent event) {}

    /**
     * Handle event when the session is unregister and the client got disconnected with server
     * @param context
     * @param event
     */
    private void handleUnregisterEvent(ChannelHandlerContext context, SocketEvent event) {}

    /**
     * Handle event when there are any exception happen
     * @param context
     * @param event
     */
    private void handleExceptionalEvent(ChannelHandlerContext context, SocketEvent event) {}
}
